import pulp
import numpy as np
import networkx as nx
import random as rnd

from resources.Application import Application
from resources.DataCentre import DataCentre
from resources.Link import Link

def get_links(network, source, target): # [TO-DO] add path cache
	links = []
	path = nx.shortest_path(G=network, source=source, target=target)
	for (n1,n2) in zip(path[0:],path[1:]):
		links.append(network.edge[n1][n2]['object'])

	return links

def opt_app_placement_discrete(applications, demand, network, epoch):
	# Application ranges
	app_range = range(len(applications))
	dc_range = range(len(network.node))

	# Compose total demand - Saves irrerations
	print "Demand: %i - Apps: %i" % ( len(demand), len(applications))

	sum_demand = dict( [ (app_name, sum([quantity for quantity in demand[str(app_name)].itervalues()]) ) for app_name in app_range])

	# Placement matrix app vs dc 
	assign_vars = pulp.LpVariable.dicts("AppPlacement", [(i, j) for i in app_range for j in dc_range], 0, 1, pulp.LpBinary)

	# Problem
	prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMinimize)

	#Cost : Objective sum(for each assigned app (cost of per resource) )
	prob += pulp.lpSum( assign_vars[(i,j)] *
					(network.node[j]['object'].calculate_cost_for_usage( applications[i].get_usage( sum_demand[i] ), 1)
					+
					sum([ link.calculate_cost_for_usage( {'NET': applications[i].get_usage( quantity )['NET']}, 1)
						for (source, quantity) in demand[str(i)].iteritems() for link in get_links(network, int(source), j)] )
					)
				for i in app_range for j in dc_range )

	# Resource constraints for resource type
	for j in dc_range: # DC
		prob += pulp.lpSum(assign_vars[(i,j)] * network.node[j]['object'].evaluate_vm_allocation( applications[i].get_resource_usage("PRODUCTION") ) for i in app_range) <= network.node[j]['object'].capacity, ''
			#print "%f: %f" % (sum([applications[i].get_usage_for_resource(sum_demand[i], resource_name)[resource_name] for i in app_range]), capacity) 

	# # Budget constraints
	# for j in dc_range: # DC
	# 	prob += pulp.lpSum(assign_vars[(i,j)] * network.node[j]['object'].evaluate_placement_cost( applications[i].get_resource_usage("PRODUCTION") )*epoch for i in app_range) <= network.node[j]['object'].budget, ''
	# 		#print "%f: %f" % (sum([applications[i].get_usage_for_resource(sum_demand[i], resource_name)[resource_name] for i in app_range]), capacity) 

	for j in dc_range: # Links
		for (n1,n2) in network.edges():
			edge = network.edge[n1][n2]['object']
			for resource_name, capacity in edge.get_properties('CAPACITY').iteritems():
	 			prob += pulp.lpSum(assign_vars[(i,j)] * applications[i].get_usage_for_resource(sum_demand[i], resource_name)[resource_name] for i in app_range) <= capacity, ''
	
	# SLA
	for i in app_range:
		for j in dc_range:
			prob += assign_vars[(i, j)] * applications[i].evaluate_sla_95th_surplus_target(network=network, target=j) >= 0.0, ''

	# Only one DC per application
	for i in app_range:
		prob += pulp.lpSum(assign_vars[(i, j)] for j in dc_range) == 1.0, ''

	## Print problem
	# print prob

	# Solve the problem
	prob.solve()

	result = {}
	TOL = 0.1

	# for app_name in app_range:
	# 	print ','.join(map(str, [assign_vars[(app_name, j)].varValue for j in dc_range]))

	for app_name in app_range: # Nasty, but need the asser
		dc_name = [j for j in dc_range if assign_vars[(app_name, j)].varValue > TOL]
		# assert len(dc_name) == 1 , "%s : %s : %s" % (app_name, dc_name, [assign_vars[(app_name, j)].varValue for j in dc_range])
		result.update( { app_name: dc_name[0] } )

	return result