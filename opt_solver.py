import pulp
import numpy as np
import networkx as nx
import random as rnd

from resources.Application import Application
from resources.DataCentre import DataCentre
from resources.Link import Link

def opt_app_placement_lists(applications, dcs, dc_capacities, dc_costs, link_cost, demand, app_util):
	# Placement matrix app vs dc 
	assign_vars = pulp.LpVariable.dicts("Placement", [(i, j) for i in applications
	for j in dcs], 0, 1, pulp.LpBinary)

	# Problem
	prob = pulp.LpProblem("AppPlacement", pulp.LpMinimize)

	# Cost: Objective sum(for each assigned app (cost of CPU + cost of IO) )
	prob += pulp.lpSum(assign_vars[(i,j)]*demand[i]*(app_util[i,r]*dc_costs[j,r]) for i in applications for j in dcs for r in range(dc_capacities.shape[1]))

	# Resource constraints for resource type
	for j in dcs: # DC
		for r in range(dc_capacities.shape[1]): # Resource
			prob += pulp.lpSum(assign_vars[(i,j)]*demand[i]*app_util[i,r] for i in applications) <= dc_capacities[j,r]

	# Only one DC per application
	for i in applications:
		prob += pulp.lpSum(assign_vars[(i, j)] for j in dcs) == 1, ''

	# Solve the problem
	prob.solve()

	#print assign_vars
	TOL = 0.00001
	for i in applications:
		print "Application ", i, " dc ", \
		[j for j in dcs if assign_vars[(i, j)].varValue > TOL]

def get_links(network, source, target): # [TO-DO] add path cache
	links = []
	path = nx.shortest_path(G=network, source=source, target=target)
	for (n1,n2) in zip(path[0:],path[1:]):
		links.append(network.edge[n1][n2]['object'])

	return links

def opt_app_placement(applications, demand, network):
	# Compose total demand - Saves irrerations
	sum_demand = [sum([quantity for quantity in demand[app_name].itervalues()]) for app_name in sorted(demand)]
	sum_demand = [sum([quantity for quantity in demand[app_name].itervalues()]) for app_name in sorted(demand, key=int)]

	# Application ranges
	app_range = range(len(applications))
	dc_range = range(len(network.node))

	# Placement matrix app vs dc 
	assign_vars = pulp.LpVariable.dicts("AppPlacement", [(i, j) for i in app_range for j in dc_range], 0, 1, pulp.LpBinary)

	# Problem
	prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMinimize)

	# Cost : Objective sum(for each assigned app (cost of per resource) )
	#prob += pulp.lpSum( assign_vars[(i,j)] * ( network.node[j]['object'].calculate_cost_for_usage( applications[i].get_usage( sum_demand[i] ), 10)) for i in app_range for j in dc_range)
	prob += pulp.lpSum( assign_vars[(i,j)] * ( 
		network.node[j]['object'].calculate_cost_for_usage( applications[i].get_usage( sum_demand[i] ), 10))
		+ (sum([ link.calculate_cost_for_usage( {'NET': applications[i].get_usage( quantity )['NET']}, 10) for link in get_links(network, int(source), j) ]) for (source, quantity) in demand[str(i)].iteritems())
		for i in app_range for j in dc_range)

	# Resource constraints for resource type
	for j in dc_range: # DC
		for resource_name, capacity in network.node[j]['object'].get_properties('CAPACITY').iteritems():
			prob += pulp.lpSum(assign_vars[(i,j)] * applications[i].get_usage_for_resource(sum_demand[i], resource_name)[resource_name] for i in app_range) <= capacity

	for j in dc_range: # Links
		for (n1,n2) in network.edges():
			edge = network.edge[n1][n2]['object']
			for resource_name, capacity in edge.get_properties('CAPACITY').iteritems():
	 			prob += pulp.lpSum(assign_vars[(i,j)]* applications[i].get_usage_for_resource(sum_demand[i], resource_name)[resource_name] for i in app_range) <= capacity
	
	# SLA
	for i in app_range:
		prob += pulp.lpSum(assign_vars[(i, j)]* max([len(get_links(network, int(source), j)) for (source, quantity) in demand[str(i)].iteritems()]) for j in dc_range) <= applications[i].sla

	# Only one DC per application
	for i in app_range:
		prob += pulp.lpSum(assign_vars[(i, j)] for j in dc_range) == 1, ''

	# Print problem
	print prob

	# Solve the problem
	prob.solve()

	#print assign_vars
	TOL = 0.00001
	for i in range(len(applications)):
		print "Application ", i, " dc ", \
		[j for j in dc_range if assign_vars[(i, j)].varValue > TOL]

# APPLICATIONS = [0,1,2,3]
# DATACENTRES = [0,1,2,3,4,5,6]
# CAPACITY = np.array([[10,10],[5,5],[5,5],[1,1],[1,1],[1,1],[1,1]])
# DEMAND = np.array(3,2,1,1) # Make dict
# APP_UTIL = np.array([[1,1],[1,1],[1,1],[1,1]])
# DC_COST = np.array([[1,1],[2,2],[2,2],[4,4],[4,4],[4,4],[4,4]])
# LINK_COST = np.array([1,1,2,2,2,2])

# opt_app_placement_list(applications=APPLICATIONS, dcs=DATACENTRES, dc_capacities=CAPACITY, dc_costs=DC_COST, link_cost=LINK_COST, demand=DEMAND, app_util=APP_UTIL)

network = nx.Graph()
network.add_node(0, object=DataCentre('DC0', DataCentre.TYPES['HUGE'], 250000.0, 10, None)) # Budget should be: 
network.add_node(1, object=DataCentre('DC1', DataCentre.TYPES['LARGE'], 250000.0, 10, None))
network.add_node(2, object=DataCentre('DC2', DataCentre.TYPES['LARGE'], 250000.0, 10, None))
network.add_node(3, object=DataCentre('DC3', DataCentre.TYPES['MEDIUM'], 20000.0, 10, None))
network.add_node(4, object=DataCentre('DC4', DataCentre.TYPES['MEDIUM'], 20000.0, 10, None))
network.add_node(5, object=DataCentre('DC5', DataCentre.TYPES['MEDIUM'], 20000.0, 10, None))
network.add_node(6, object=DataCentre('DC6', DataCentre.TYPES['MEDIUM'], 20000.0, 10, None))

network.add_edge(0, 1, object=Link('L0', Link.TYPES['SMALL'], float('inf'), 10.0, None))
network.add_edge(0, 2, object=Link('L1', Link.TYPES['SMALL'], float('inf'), 10.0, None))
network.add_edge(1, 3, object=Link('L2', Link.TYPES['SMALL'], float('inf'), 10.0, None))
network.add_edge(1, 4, object=Link('L3', Link.TYPES['SMALL'], float('inf'), 10.0, None))
network.add_edge(2, 5, object=Link('L4', Link.TYPES['SMALL'], float('inf'), 10.0, None))
network.add_edge(2, 6, object=Link('L5', Link.TYPES['SMALL'], float('inf'), 10.0, None))

		# Create applications
APPLICATIONS = [Application(i, Application.TYPES[rnd.choice(Application.TYPES.keys())], None, 'MAX_BUDGET_SURPLUS') for i in range(0,4)]
DATACENTRES = [0,1,2,3,4,5,6]
CAPACITY = np.array([[10,10],[5,5],[5,5],[1,1],[1,1],[1,1],[1,1]])
DEMAND = {'0':{'3':1}, '1':{'3':1}, '2':{'3':1},'3':{'3':1}} # Make dict
APP_UTIL = np.array([[1,1],[1,1],[1,1],[1,1]])
DC_COST = np.array([[1,1],[2,2],[2,2],[4,4],[4,4],[4,4],[4,4]])
LINK_COST = np.array([1,1,2,2,2,2])

opt_app_placement(applications=APPLICATIONS, demand=DEMAND, network=network)
