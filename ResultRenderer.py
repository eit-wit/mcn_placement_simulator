import numpy as np
import matplotlib.pyplot as plt

from mpl_toolkits.mplot3d import Axes3D
#import statsmodels.api as sm # recommended import according to the docs

class ResultRenderer(object):

	def __init__(self, dc_depth_map):
		self.dc_depth_map = dc_depth_map

		self.constants = {}

		self.CONSTANTS = {'DEMAND_TYPE': [self.constant_mean]}

		self.MEASUREMENTS = {	
								'UTILISATION_DC': [#self.render_mean_ts, 
												#self.render_min_ts, 
												#self.render_max_ts, 
												self.render_per_depth_ts,
												#self.render_per_depth_cdf
												],
		# 						#'UTILISATION_LINK': [
		# 										#self.render_mean_ts, 
		# 										#self.render_min_ts, 
		# 										#self.render_max_ts
		# 						#				],
		# 						'BUDGET_SURPLUS': [
		# 										#self.render_mean_ts, 
		# 										#self.render_min_ts, 
		# 										#self.render_max_ts, 
		# 										self.render_per_depth_ts
		# 										],
		# 						'BUDGET_SURPLUS_ESTIMATED': [
		# 										#self.render_mean_ts, 
		# 										#self.render_min_ts, 
		# 										#self.render_max_ts, 
		# 										self.render_per_depth_ts
		# 										],
		# 						'PERMUTABILITY': [
		# 										self.render_per_depth_ts
		# 										],
		# 						'SLA_SURPLUS': [self.render_per_type_ts],
		# 						'ALLOCATION_SYSTEM': [self.render_ts],
		# 						'ALLOCATION': [self.render_mean_ts,
		# 						#				self.render_max_ts,
		# 										self.render_per_depth_ts,
		# 										#self.render_per_depth_cdf
		# 										],
		# 						'PLACEMENT': [self.render_mean_ts, 
		# 										#self.render_per_type_ts
		# 										],
		 						'SLA_VILOATIONS': [self.render_cumsum_ts_array],
		 						'SLA_DURATION': [self.render_per_type_ts],
		 						'BUDGET_VILOATIONS': [self.render_cumsum_ts_array],
		# 						'NET_APP_FLOW': [self.render_per_depth_ts],
		# 						'NET_UTIL_FLOW': [self.render_per_depth_ts],
								#'EPOCH_DURATION': [self.render_cumsum_ts],
								'COST': [self.render_sum_ts_relative, 
										self.render_sum_ts
										#self.render_cumsum_ts
										],
								# 'MIGRATING': [self.render_cumsum_ts]
				# 'SLA_SURPLUS': '',
				# 'MIGRATING': '',
				# 'PLACEMENT': ''
				}
	def render_all(self, data_set):
		for meas_name, const_funcs in self.CONSTANTS.iteritems(): 
			for func in const_funcs:
				func(meas_name, data_set)

		index = 0
		for meas_name, meas_funcs in sorted(self.MEASUREMENTS.items()):
			for func in meas_funcs:
				func(index, meas_name, data_set)

				index += 1
		plt.show()

	def constant_mean(self, meas_name, data_set):
		self.constants[meas_name] = np.mean(data_set.itervalues().next()[meas_name], axis=0)

	def render_per_type_ts(self, index, meas_name, data_set):
		fig = plt.figure(index)

		demand_type_map = self.constants['DEMAND_TYPE']

		types = set(demand_type_map)
		nbr_types = len(types)

		for scenario_name, data in sorted(data_set.items()):
			result = dict((i,[]) for i in types)
			times = range(data[meas_name].shape[0])
			
			for t in times:

				utils_at_t = {}
				for i in range(data[meas_name].shape[1]):
					demand_type = demand_type_map[i]

					if demand_type not in utils_at_t:
						utils_at_t[demand_type] = []

					utils_at_t[demand_type].append(data[meas_name][t,i])

				for demand_type in types:
					values = utils_at_t[demand_type]
					if len(values) == 0:
						result[demand_type].append(0)
					else:
						result[demand_type].append( np.mean(values) )

			for demand_type in types:
				plt.subplot( int('%i1%i'%(nbr_types, demand_type)) ).plot(times, result[demand_type])
				plt.subplot( int('%i1%i'%(nbr_types, demand_type)) ).set_title("%s - Demand type %i" % (meas_name.replace('_',' '), demand_type) )
				plt.subplot( int('%i1%i'%(nbr_types, demand_type)) ).hold(True)
				plt.subplot( int('%i1%i'%(nbr_types, demand_type)) ).grid()

	def render_per_depth_ts(self, index, meas_name, data_set):
		fig = plt.figure(index)

		depths = set(self.dc_depth_map.values())
		nbr_depths = len(depths)

		for scenario_name, data in sorted(data_set.items()):
			result = dict((i,[]) for i in depths)
			times = range(data[meas_name].shape[0])
			
			for t in times:

				utils_at_t = {}
				for i in range(data[meas_name].shape[1]):
					depth = self.dc_depth_map[i]

					if depth not in utils_at_t:
						utils_at_t[depth] = []

					utils_at_t[depth].append(data[meas_name][t,i])

				for depth in depths:
					values = utils_at_t[depth]
					if len(values) == 0:
						result[depth].append(0)
					else:
						result[depth].append( np.mean(values) )

			for depth in depths:
				plt.subplot( int('%i1%i'%(nbr_depths, depth+1)) ).plot(times, result[depth])
				plt.subplot( int('%i1%i'%(nbr_depths, depth+1)) ).set_title("%s - Depth %i" % (meas_name.replace('_',' '), depth) )
				plt.subplot( int('%i1%i'%(nbr_depths, depth+1)) ).hold(True)
				plt.subplot( int('%i1%i'%(nbr_depths, depth+1)) ).grid()

	def render_per_depth_cdf(self, index, meas_name, data_set):
		fig = plt.figure(index)

		depths = set(self.dc_depth_map.values())
		nbr_depths = len(depths)

		data_at_depth = dict([(i,[]) for i in range(nbr_depths)])

		for scenario_name, data in sorted(data_set.items()):
			result = dict((i,[]) for i in depths)
			times = range(data[meas_name].shape[0])
			end_time = max(times)
			
			for i in range(data[meas_name].shape[1]):
				data_at_depth[self.dc_depth_map[i]].append(data[meas_name][end_time,i])

			for depth in depths:
				counts, bin_edges = np.histogram(data_at_depth[depth], bins=20, normed=False)
				sum_counts = sum(counts)
				cdf = np.cumsum(counts/sum_counts)
				
				plt.subplot( int('%i1%i'%(nbr_depths, depth+1)) ).plot(bin_edges[1:], cdf)
				plt.subplot( int('%i1%i'%(nbr_depths, depth+1)) ).set_title("%s - Depth %i" % (meas_name.replace('_',' '), depth) )
				plt.subplot( int('%i1%i'%(nbr_depths, depth+1)) ).hold(True)
				plt.subplot( int('%i1%i'%(nbr_depths, depth+1)) ).grid()

	def render_hist(self, index, meas_name, data_set):
		plt.figure(index)

		for scenario_name, data in data_set.iteritems():
			plt.hist(data[meas_name].flatten(), data[meas_name].shape[1], normed=False, label=scenario_name, alpha=0.5)

			plt.hold(True)

		plt.grid()
		plt.legend(loc='best')
		plt.title(meas_name.replace('_',' '))
		plt.xlabel('Data centre')
		plt.hold(False)

	def render_cdf(self, index, meas_name, data_set):
		num_bins = 100

		plt.figure(index)

		for scenario_name, data in sorted(data_set.items()):
			dim = len(data[meas_name].flatten())
			counts, bin_edges = np.histogram(data[meas_name], bins=num_bins, normed=False)
			cdf = np.cumsum(counts)
			plt.plot(bin_edges[1:], np.true_divide(cdf,dim), label=scenario_name)

			plt.hold(True)

		plt.grid()
		plt.legend(loc='best')
		plt.title(meas_name.replace('_',' '))
		plt.xlabel('Percentage')
		plt.hold(False)

	def render_wireframe_cdf_time(self, index, meas_name, data_set):
		num_bins = 100

		fig = plt.figure(index)
		ax = fig.gca(projection='3d')

		for scenario_name, data in sorted(data_set.items()):
			data_set = data[meas_name]
			x_dim = num_bins
			y_dim = data_set.shape[0]
			
			X, Y = np.meshgrid(np.arange(x_dim), np.arange(y_dim))

			Z = np.empty((0, x_dim), float)

			for i in data_set:
				counts, bin_edges = np.histogram(i, bins=num_bins, normed=False)
				cdf = np.cumsum(counts)
				result = np.true_divide(cdf,len(i))

				Z = np.append(Z,result)

			Z = np.reshape(Z, (x_dim, y_dim))

			ax.plot_wireframe(X, Y, Z)

			plt.hold(True)

		plt.grid()
		plt.legend(loc='best')
		plt.title(meas_name.replace('_',' '))
		plt.xlabel('Percentage')
		plt.ylabel('Time')
		plt.hold(False)

	def render_wireframe_time(self, index, meas_name, data_set):
		num_bins = 100

		fig = plt.figure(index)
		ax = fig.gca(projection='3d')

		for scenario_name, data in sorted(data_set.items()):
			data_set = data[meas_name]
			x_dim = data_set.shape[1]
			y_dim = data_set.shape[0]
			
			X, Y = np.meshgrid(np.arange(x_dim), np.arange(y_dim))

			#Z = np.empty((0, x_dim), float)

			Z = data_set

			ax.plot_wireframe(X, Y, Z)

			plt.hold(True)

		plt.grid()
		plt.legend(loc='best')
		plt.title(meas_name.replace('_',' '))
		plt.xlabel('Percentage')
		plt.ylabel('Time')
		plt.hold(False)

	def render_mean_ts(self, index, meas_name, data_set):
		plt.figure(index)

		for scenario_name, data in sorted(data_set.items()):
			a = np.mean(data[meas_name], axis=1)
			t = np.arange(len(a))

			plt.plot(t, a, label=scenario_name)
			plt.hold(True)

		plt.grid()
		plt.legend(loc='best')
		plt.title("Mean - %s" % meas_name.replace('_',' '))
		plt.xlabel('Time')
		plt.hold(False)

	def render_min_ts(self, index, meas_name, data_set):
		plt.figure(index)

		for scenario_name, data in sorted(data_set.items()):
			a = np.min(data[meas_name], axis=1)
			t = np.arange(len(a))

			plt.plot(t, a, label=scenario_name)
			plt.hold(True)

		plt.grid()
		plt.legend(loc='best')
		plt.title("Min - %s" % meas_name.replace('_',' '))
		plt.xlabel('Time')
		plt.hold(False)

	def render_max_ts(self, index, meas_name, data_set):
		plt.figure(index)

		for scenario_name, data in sorted(data_set.items()):
			a = np.max(data[meas_name], axis=1)
			t = np.arange(len(a))

			plt.plot(t, a, label=scenario_name)
			plt.hold(True)

		plt.grid()
		plt.legend(loc='best')
		plt.title("Max - %s" % meas_name.replace('_',' '))
		plt.xlabel('Time')
		plt.hold(False)

	def render_mean_div_max_ts(self, index, meas_name, data_set):
		plt.figure(index)

		for scenario_name, data in sorted(data_set.items()):
			a = np.max(data[meas_name], axis=1)
			#m = np.max(data[meas_name], axis=1)

			#a = np.true_divide(a,m)
			t = np.arange(len(a))

			plt.plot(t, a, label=scenario_name)
			plt.hold(True)

		plt.grid()
		plt.legend(loc='best')
		plt.title(meas_name.replace('_',' '))
		plt.xlabel('Time')
		plt.hold(False)

	def render_sum_ts(self, index, meas_name, data_set):
		plt.figure(index)

		for scenario_name, data in sorted(data_set.items()):
			a = np.sum(data[meas_name], axis=1)
			t = np.arange(len(a))

			plt.plot(t, a, label=scenario_name)
			plt.hold(True)

		plt.grid()
		plt.legend(loc='best')
		plt.title(meas_name.replace('_',' '))
		plt.xlabel('Time')
		plt.hold(False)

	def render_sum_ts_relative(self, index, meas_name, data_set, relative_to='OPTIMAL'):
		plt.figure(index)

		# Extract relative data
		data_relative = data_set[relative_to][meas_name]
		relative_a = np.sum(data_relative, axis=1)

		targeted_sets = [scenario_name for scenario_name in sorted(data_set) if scenario_name != relative_to]

		#for scenario_name, data in sorted(data_set.items()):
		for scenario_name in targeted_sets:
			a = 1.0 - np.true_divide(np.sum(data_set[scenario_name][meas_name], axis=1), relative_a)
			t = np.arange( len(a) )

			plt.plot(t, a, label=scenario_name)
			plt.hold(True)

		plt.grid()
		plt.legend(loc='best')
		plt.title(meas_name.replace('_',' '))
		plt.xlabel('Time')
		plt.hold(False)

	def render_cumsum_ts(self, index, meas_name, data_set):
		plt.figure(index)

		for scenario_name, data in sorted(data_set.items()):
			a = np.sum(data[meas_name], axis=1)
			a = np.cumsum(a)
			t = np.arange(len(a))

			plt.plot(t, a, label=scenario_name)
			plt.hold(True)

		plt.grid()
		plt.legend(loc='best')
		plt.title(meas_name.replace('_',' '))
		plt.xlabel('Time')
		plt.hold(False)

	def render_cumsum_ts_array(self, index, meas_name, data_set):
		plt.figure(index)

		for scenario_name, data in sorted(data_set.items()):
			a = np.cumsum(data[meas_name])
			t = np.arange(len(a))

			plt.plot(t, a, label=scenario_name)
			plt.hold(True)

		plt.grid()
		plt.legend(loc='best')
		plt.title(meas_name.replace('_',' '))
		plt.xlabel('Time')
		plt.hold(False)

	def render_ts(self, index, meas_name, data_set):
		plt.figure(index)

		for scenario_name, data in sorted(data_set.items()):
			t = np.arange(len(data[meas_name]))
			plt.plot(t, data[meas_name], label=scenario_name)
			plt.hold(True)

		plt.grid()
		plt.legend(loc='best')
		plt.title(meas_name.replace('_',' '))
		plt.xlabel('Time')
		plt.hold(False)

	def render_count_changes_tc(self, index, meas_name, data_set):
		plt.figure(index)

		for scenario_name, data in sorted(data_set.items()):
			a = np.diff(data[meas_name], axis=0)
			a = np.apply_along_axis(np.count_nonzero, 0, a)
			t = np.arange(len(a))

			plt.plot(t, a, label=scenario_name)
			plt.hold(True)

		plt.grid()
		plt.legend(loc='best')
		plt.title(meas_name.replace('_',' '))
		plt.xlabel('')
		plt.hold(False)

	def render_count_changes_hist(self, index, meas_name, data_set):
		plt.figure(index)

		for scenario_name, data in sorted(data_set.items()):
			a = np.diff(data[meas_name], axis=0)
			a = np.apply_along_axis(np.count_nonzero, 0, a)
			t = np.arange(len(a))

			plt.hist(np.true_divide(a,t), t, normed=False, label=scenario_name)
			plt.hold(True)

		plt.grid()
		plt.legend(loc='best')
		plt.title(meas_name.replace('_',' '))
		plt.xlabel('Time')
		plt.hold(False)

	def runningMeanFast(x, N):
		return np.convolve(x, np.ones((N,))/N)[(N-1):]