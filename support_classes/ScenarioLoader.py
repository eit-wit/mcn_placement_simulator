import yaml

class ScenarioLoader(object):

	def __init__(self, batch_file_path):
		self.batch = yaml.load( open(batch_file_path) )

		self.batch_index = -1

	def get_configuration(self):		
		current_configuration = {}

		for batch_index in range(len(self.batch)):
			self.batch_index = batch_index

			configuration_delta = self.batch[self.batch_index]["CONFIGURATION"]

			current_configuration.update(configuration_delta)

			yield (current_configuration)

	def get_scenario(self):
		current_scenario = {}

		for scenario_delta in self.batch[self.batch_index]["SCENARIOS"]:

			current_scenario.update(scenario_delta)

			yield (current_scenario)