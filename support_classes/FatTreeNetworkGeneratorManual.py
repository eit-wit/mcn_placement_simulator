import networkx as nx
import math
import matplotlib.pyplot as plt
import numpy as np

from support_classes.TreeNetworkGeneratorManual import TreeNetworkGeneratorManual
from resources.DataCentre_Discrete import DataCentre_Discrete
from resources.Link import Link

class FatTreeNetworkGeneratorManual(TreeNetworkGeneratorManual):

	def __init__(self, config, dc_types_map, link_type_map):
		TreeNetworkGeneratorManual.__init__(self, config, dc_types_map, link_type_map)
		self.level = config['LEVEL']

		assert self.level > min([level_id for level_id in self.dc_types_map])

	def generate_network(self):
		''' Instantiate network '''
		self.network = self.generate_graph(self.tree_spread, self.tree_depth)

		self.nbr_nodes = len(self.network.node)

		print "\t- Graph Generated -"
		print "\t %i nodes" % self.nbr_nodes
		print "\t Depth: %i" % self.tree_depth
		print "\t Spread: %i" % self.tree_spread

		''' Assign node '''
		self.assign_dc_type()

		''' Fatten tree '''
		# Find level id
		level_id = [type_id for type_id, type_name in self.dc_types_map.iteritems() if type_name == self.level][0]
		# Collect all nodes at this level
		nodes_at_level = [node_id for node_id, node in self.network.node.iteritems() if node['type']==level_id]
		# Count number of nodes at level
		nbr_nodes_at_level = len(nodes_at_level)
		# Collect nodes at sub-level
		nodes_at_sub_level = [node_id for node_id, node in self.network.node.iteritems() if node['type']==level_id-1]
		# Count number of nodes at sub-level
		nbr_nodes_at_sub_level = len(nodes_at_sub_level)

		# Connect all nodes at sub-level 
		for index in range(nbr_nodes_at_sub_level-1):
			peer1 = nodes_at_sub_level[ index ]
			peer2 = nodes_at_sub_level[ index+1 ] #%nbr_nodes_at_sub_level ]

			self.network.add_edge(peer1,peer2)

		vertical_connections = []
		# Connect nodes at level
		for index in range(nbr_nodes_at_level-1):
			# Horizontal connectsions
			node_id = nodes_at_level[index]
			neighbour = nodes_at_level[(index+1)] #%nbr_nodes_at_level]

			self.network.add_edge(node_id, neighbour) # Horizontal connections at level

			# Veritcal connections
			# my_neighbours = [n for n in self.network.neighbors(node_id) if self.network.node[n]['type'] < level_id]# if self.network.node[my_neighbour]['type']}
			# their_neighbours = [n for n in self.network.neighbors(neighbour) if self.network.node[n]['type'] < level_id] #if self.network.node[their_neighbour]['type']}

			# vertical_connections += [ (node_id, n2) for n1 in my_neighbours for n2 in their_neighbours if n1 in self.network.neighbors(n2)]

		# for (n1, n2) in vertical_connections:
		# 	self.network.add_edge(n1, n2)

		''' Assign link types '''
		self.assign_link_type()

		#self.display_graph()

		''' Neighbourlyness stats '''
		neighbourlyness = {}
		print self.nodes_assignment
		for dc_type, peers in self.nodes_assignment.iteritems():
			neighbourlyness[dc_type] = []
			nbr_nodes_of_type = len(peers)
			peers = list(peers)
			for node_index in range(max(nbr_nodes_of_type-1,1)):
				neighbourlyness[dc_type].append(
					len(nx.shortest_path(G=self.network, source=peers[node_index], target=peers[(node_index+1)%nbr_nodes_of_type]))-1
					)

		print "\t- neighbourlyness -"
		for depth, values in neighbourlyness.iteritems():
			print "\t Type: %s" % self.dc_types_map[depth]
			print "\t \t Mean distance to peers: %f" % np.mean(values)
			print "\t \t Max distance to peers: %f" % max(values)
			print "\t \t Min distance to peers: %f" % min(values) 

		return self.network
