import networkx as nx
import math
import matplotlib.pyplot as plt

from support_classes.NetworkGenerator import NetworkGenerator
from resources.DataCentre_Discrete import DataCentre_Discrete
from resources.Link import Link

class TreeNetworkGeneratorManual(NetworkGenerator):

	def __init__(self, config, dc_types_map, link_type_map):
		NetworkGenerator.__init__(self, config, dc_types_map, link_type_map)
		
		self.nbr_nodes = config['NBR_NODES']
		self.small_edge_threshold = config['SMALL_EDGE_THRESHOLD']		

		self.tree_depth = len(dc_types_map)-1
		self.tree_spread = int( math.ceil( math.pow(self.nbr_nodes+1, 1.0/(self.tree_depth+1) ) ) )

	def generate_network(self):
		''' Instantiate network '''
		self.network = self.generate_graph(self.tree_spread, self.tree_depth)

		self.nbr_nodes = len(self.network.node)

		print "\t- Graph Generated -"
		print "\t %i nodes" % self.nbr_nodes
		print "\t Depth: %i" % self.tree_depth
		print "\t Spread: %i" % self.tree_spread

		''' Assign node and edge types '''
		self.assign_dc_type()
		self.assign_link_type()

		#self.display_graph()

		return self.network

	def generate_graph(self, tree_spread, tree_depth):
		return nx.balanced_tree(tree_spread, tree_depth)

	def assign_dc_type(self):
		'''
		Assign type
		'''
		''' Data centres '''
		self.nodes_assignment[0] = set([node_id for node_id in self.network.node if nx.degree(G=self.network,nbunch=node_id) <= self.small_edge_threshold ])

		for node_id in self.nodes_assignment[0]:
			self.network.node[node_id]['type'] = 0

		self.assign_dc_type_recursive(node_assignment=self.nodes_assignment, dc_level=0, until_level=self.tree_depth)

		assert len([1 for node in self.network.node.itervalues() if 'type' not in node]) == 0, "There are unassigned nodes in the network."

		print "\t- Node assignment -"
		for type_id, node_type in self.dc_types_map.iteritems():
			print "\t Number of %s nodes: %i/%i"  % (node_type, len(self.nodes_assignment[type_id]), self.nbr_nodes)

	def assign_link_type(self):
		edges_type = {dc_type:[] for dc_type in self.dc_types_map}

		for node_id in self.network.node:
			dc_type = self.network.node[node_id]['type']
			for neighbour in self.network.neighbors(node_id):
				neighbor_dc_type = self.network.node[neighbour]['type']

				link_type = min(min(dc_type,neighbor_dc_type),2)

				#G.edge[min(node_id,neighbour)][max(node_id,neighbour)]['type'] = link_type
				self.network.edge[node_id][neighbour]['type'] = link_type

				edges_type[link_type].append( (min(node_id,neighbour), max(node_id,neighbour) ) )

	def assign_dc_type_recursive(self, node_assignment, dc_level, until_level):
		if dc_level < until_level:
			for node in node_assignment[dc_level]:
				for neighbour in self.network.neighbors(node):
					if 'type' not in self.network.node[neighbour]:
						#print "%i -> %i" % (node, neighbour)
						node_assignment[dc_level+1].add(neighbour)
						self.network.node[neighbour]['type'] = dc_level+1

			self.assign_dc_type_recursive(node_assignment=node_assignment, dc_level=dc_level+1, until_level=until_level)