import json
import sys
import yaml
import logging

class WorkloadSource(object):

	def __init__(self, file_name):
		self.data = yaml.load(open(file_name))
		
		self.workload = self.data['WORKLOAD']
		self.stats = self.data['STATS']

	# Produce workload
	def next(self):
		for t in sorted(self.workload, key=lambda x: float(x)):
			yield (float(t), self.workload[t])

	# Get stats
	def get_stats(self):
		return self.stats["MAX_APP_SPREAD"], self.stats["SLA_PER_TYPE"]

	# Get the last time stamp in the workload
	def get_workload_time_span(self):
		return float(sorted(self.workload, key=lambda x: int(x))[-1])+1