import networkx as nx
import matplotlib.pyplot as plt
from resources.DataCentre_Discrete import DataCentre_Discrete
from resources.Link import Link
#from networkx import graphviz_layout

class NetworkGenerator(object):

	def __init__(self, config, dc_types_map, link_type_map):
		self.config = config
		self.dc_types_map = dc_types_map
		self.nbr_dc_types = len(self.dc_types_map)
		self.link_type_map = link_type_map
		self.nbr_link_types = len(self.link_type_map)

		self.nodes_assignment = {type_id:set() for type_id in self.dc_types_map}

		self.network = None
		self.nbr_nodes = 0

	def generate_network(self):
		raise NotImplementedError

	def assign_dc_type(self):
		raise NotImplementedError

	def assign_link_type(self):
		edges_type = {dc_type:[] for dc_type in self.dc_types_map}

		for node_id in self.network.node:
			dc_type = self.network.node[node_id]['type']
			for neighbour in self.network.neighbors(node_id):
				neighbor_dc_type = self.network.node[neighbour]['type']

				link_type = min(min(dc_type,neighbor_dc_type),2)

				#G.edge[min(node_id,neighbour)][max(node_id,neighbour)]['type'] = link_type
				self.network.edge[node_id][neighbour]['type'] = link_type

				edges_type[link_type].append( (min(node_id,neighbour), max(node_id,neighbour) ) )

	def initialise_nodes(self, budget_factor=0.8, epoch=10, expulsion_strategy="LOCAL_BEST_EFFORT_EXP"):
		# Nodes
		for node_index, node in self.network.node.iteritems():
			node_type = self.dc_types_map[node['type']]
			node['object'] = DataCentre_Discrete(node_index, DataCentre_Discrete.TYPES[node_type], DataCentre_Discrete.VM_TYPES['SMALL'], budget_factor, epoch, expulsion_strategy)

		# Edges
		edge_index = 0
		for (n1,n2) in self.network.edges():
			edge = self.network.edge[n1][n2]
			edge_type = self.link_type_map[ edge['type'] ]
			edge['object'] = Link(edge_index, Link.TYPES['SMALL'], budget_factor, epoch, 'NONE')

		return self.network

	def get_leaf_nodes(self):
		return list(self.nodes_assignment[0])

	def get_dc_depth_map(self):
		return {node_id:node['type'] for node_id, node in self.network.node.iteritems()}

	def get_root_nodes(self):
		return list(self.nodes_assignment[3])

	def get_nbr_nodes(self):
		return len(self.network.node)

	def display_graph(self,save=False):
		pos=nx.circular_layout(self.network)
		#pos=nx.graphviz_layout(self.network,prog='twopi',args='')


		# TO-DO make this general 
		''' Huge '''
		nx.draw_networkx_nodes(self.network,pos,
		                       nodelist=[dc_id for dc_id, node in self.network.node.iteritems() if node['type'] == 3],
		                       node_color='r',
		                       node_size=3000,
		                   		alpha=0.8)
		''' Large '''
		nx.draw_networkx_nodes(self.network,pos,
		                       nodelist=[dc_id for dc_id, node in self.network.node.iteritems() if node['type'] == 2],
		                       node_color='g',
		                       node_size=1500,
		                   		alpha=0.8)
		''' Medium '''
		nx.draw_networkx_nodes(self.network,pos,
		                       nodelist=[dc_id for dc_id, node in self.network.node.iteritems() if node['type'] == 1],
		                       node_color='b',
		                       node_size=750,
		                  		alpha=0.8)
		''' Small '''
		nx.draw_networkx_nodes(self.network,pos,
		                       nodelist=[dc_id for dc_id, node in self.network.node.iteritems() if node['type'] == 0],
		                       node_color='y',
		                       node_size=375,
		                   alpha=0.8)

		''' Large '''
		nx.draw_networkx_edges(self.network,pos,
		                       edgelist=[(n1,n2) for (n1,n2) in self.network.edges() if self.network.edge[n1][n2]['type'] == 2],
		                       width=8,alpha=0.5,edge_color='r')
		''' Medium '''
		nx.draw_networkx_edges(self.network,pos,
		                       edgelist=[(n1,n2) for (n1,n2) in self.network.edges() if self.network.edge[n1][n2]['type'] == 1],
		                       width=4,alpha=0.5,edge_color='g')
		''' Small '''
		nx.draw_networkx_edges(self.network,pos,
		                       edgelist=[(n1,n2) for (n1,n2) in self.network.edges() if self.network.edge[n1][n2]['type'] == 0],
		                       width=2,alpha=0.5,edge_color='b')

		nx.draw_networkx_labels(self.network ,pos, {i:i for i in range(0,self.nbr_nodes)}, font_size=13)

		plt.axis('off')

		plt.savefig("network_graph.png", dpi=600)
		plt.savefig('network_graph.eps', format='eps')
		plt.savefig('network_graph.svg', format='svg')

		plt.show()
