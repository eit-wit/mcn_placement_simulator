import networkx as nx
from support_classes.NetworkGenerator import NetworkGenerator
from resources.DataCentre_Discrete import DataCentre_Discrete
from resources.Link import Link
import matplotlib.pyplot as plt
import numpy as np

class ErdosRenyiNetworkGenerator(NetworkGenerator):

	def __init__(self, config, dc_types_map, link_type_map):
		NetworkGenerator.__init__(self, config, dc_types_map, link_type_map)
		self.level = config['LEVEL']
		self.p = config['P']

		assert self.level > min([level_id for level_id in self.dc_types_map])

		self.nbr_nodes = config['NBR_NODES']
		self.node_degrees = []
		self.node_depths = []
		self.node_degrees = []

		''' Properties '''
		self.node_type_distributsions = [0.425, 0.2, 0.05]
		self.nbr_types_dist = len(self.node_type_distributsions)
		self.range_types_dist = range(self.nbr_types_dist)

	def generate_network(self):
		''' Generate graph '''
		valid_graph = False

		''' Meet conditions '''
		while not valid_graph:
			self.network = nx.erdos_renyi_graph(self.nbr_nodes, self.p)
			self.node_degrees = [nx.degree(G=self.network, nbunch=n) for n in self.network.node]
			
			valid_graph = min(self.node_degrees) > 0
			print "Has no disconnected nodes: %s" % valid_graph
		
			if valid_graph:
				''' Assign node and edge types '''
				self.assign_dc_type()

				valid_graph *= len(self.nodes_assignment[self.nbr_dc_types-1]) > 0 #and len(self.nodes_assignment[self.nbr_dc_types-1]) <= 4
				print "Has root nodes: %s" % bool(valid_graph)

				if valid_graph:
					self.node_depths = [ min([ len(nx.shortest_path(G=self.network, source=node, target=root)) for root in self.nodes_assignment[self.nbr_dc_types-1]]) for node in range(self.nbr_nodes)] 
					valid_graph *= max(self.node_depths) == 3
					print "Has depth 3: %s" % bool(valid_graph)

		''' Fatten '''
		level_id = [type_id for type_id, type_name in self.dc_types_map.iteritems() if type_name == self.level][0]
		nodes_at_level = [node_id for node_id, node in self.network.node.iteritems() if node['type']==level_id]
		nbr_nodes_at_level = len(nodes_at_level)

		nodes_at_sub_level = [node_id for node_id, node in self.network.node.iteritems() if node['type']==level_id-1]
		nbr_nodes_at_sub_level = len(nodes_at_sub_level)

		# for index in range(nbr_nodes_at_sub_level):
		# 	peer1 = nodes_at_sub_level[ index ]
		# 	peer2 = nodes_at_sub_level[ (index+1)%nbr_nodes_at_sub_level ]

		# 	self.network.add_edge(peer1,peer2)

		for index in range(nbr_nodes_at_level):
			peer1 = nodes_at_level[ index ]
			peer2 = nodes_at_level[ (index+1)%nbr_nodes_at_level ]

			self.network.add_edge(peer1,peer2)

		roots = list(self.nodes_assignment[3])
		for index in range(len(roots)):
			peer1 = roots[ index ]
			peer2 = roots[ (index+1)%len(roots) ]

			self.network.add_edge(peer1,peer2)

		# vertical_connections = []
		# # Vertical connections
		# for index in range(nbr_nodes_at_level):
		# 	node_id = nodes_at_level[index]
		# 	neighbour = nodes_at_level[(index+1)%nbr_nodes_at_level]

		# 	self.network.add_edge(node_id, neighbour) # Horizontal connections at level

		# 	my_neighbours = [n for n in self.network.neighbors(node_id) if self.network.node[n]['type'] <= level_id]# if self.network.node[my_neighbour]['type']}
		# 	their_neighbours = [n for n in self.network.neighbors(neighbour) if self.network.node[n]['type'] <= level_id] #if self.network.node[their_neighbour]['type']}

		# 	vertical_connections += [ (node_id, n2) for n1 in my_neighbours for n2 in their_neighbours if n1 in self.network.neighbors(n2)]

		# for (n1, n2) in vertical_connections:
		# 	self.network.add_edge(n1, n2)

		self.assign_link_type()

		''' Neighbourlyness stats '''
		neighbourlyness = {}
		for dc_type, peers in self.nodes_assignment.iteritems():
			neighbourlyness[dc_type] = []
			nbr_nodes_of_type = len(peers)
			peers = list(peers)
			for node_index in range(nbr_nodes_of_type):
				links = nx.shortest_path(G=self.network, source=peers[node_index], target=peers[(node_index+1)%nbr_nodes_of_type])
				neighbourlyness[dc_type].append(len(links)-1)

		print "\t- neighbourlyness -"
		for depth, values in neighbourlyness.iteritems():
			print "\t Type: %s" % self.dc_types_map[depth]
			print "\t \t Mean distance to peers: %f" % np.mean(values)
			print "\t \t Max distance to peers: %f" % max(values)
			print "\t \t Min distance to peers: %f" % min(values) 

		#self.display_graph()

		print "\t- Graph Generated -"
		print "\t %i nodes" % self.nbr_nodes
		print "\t Max degrees: %i" % max(self.node_degrees)
		print "\t Max depth: %i" % max(self.node_depths)

		print "\t- DC types Generated -"
		for index, name in self.dc_types_map.iteritems():
			print "\t %s: %i nodes" % (name, len(self.nodes_assignment[index]))

		return self.network

	def assign_dc_type(self):
		# Ingredients
		self.node_degrees = [nx.degree(G=self.network, nbunch=n) for n in self.network.node]
		degrees = list(sorted(set(self.node_degrees)))
		degree_count = { x: self.node_degrees.count(x) for x in degrees}
		nbr_degrees = len(degree_count)

		# Tools
		thlds = [0 for i in range(self.nbr_types_dist+2)]
		count_per_type =[0 for i in self.range_types_dist]
		degree_index = 0
		type_index = 0

		''' Assign thresholds '''
		for type_index in self.range_types_dist:
			count_per_type[type_index] = 0
			target_nbr_nodes = self.node_type_distributsions[type_index]*self.nbr_nodes

			while count_per_type[type_index] <= target_nbr_nodes and degree_index < nbr_degrees:
				degree = degrees[degree_index]

				thlds[type_index+1] = degree
				count_per_type[type_index] += degree_count[degree]
				
				degree_index +=1

		thlds[-1] = max(degrees)

		''' Apply thresholds and assign types '''
		self.nodes_assignment = {type_id:set() for type_id in self.dc_types_map}

		for type_id in range(self.nbr_dc_types):
			nodes = [node for node in self.network.node if nx.degree(G=self.network, nbunch=node) > thlds[type_id] and nx.degree(G=self.network, nbunch=node) <= thlds[type_id+1] ]
			self.nodes_assignment[type_id] = set(nodes)
			for node_id in nodes:
				self.network.node[node_id]['type'] =  type_id