import numpy as np
import numpy.random as rnd
from scipy.stats import norm
import json
import pulp
import networkx as nx
import sys

from scipy.stats import zipf
from numpy import linspace

'''
Application generation classes
'''
class app_gen(object):

	def __init__(self, init_const, demand_cap, leaf_range, boundary_bounce, move_after):
		self.init_const = init_const
		self.demand_cap = demand_cap
		self.leaf_range = leaf_range
		self.boundary_bounce = boundary_bounce
		self.move_after = move_after + np.random.sample()*move_after

		self.iterations = 0

		# Propoerties
		self.total_demand = sum([demand for demand in self.init_const.itervalues()])
		self.spread = float(len(init_const))
		self.spread_per_side = self.spread/2.0
		self.nbr_leafs = len(leaf_range)
		self.min_leaf_index = 0
		self.max_leaf_index = len(leaf_range)-1

		self.leaf_index_range = range( len(leaf_range) )

		leafs_index = [leaf_range.index(leaf) for leaf, demand in self.init_const.iteritems() if demand >0.0]

		self.position_index = min(leafs_index) + (max(leafs_index)-min(leafs_index))/2

		#print "leaf_index: %s, min:%i max:%i, position index %i" % (leafs_index, min(leafs_index), max(leafs_index), self.position_index)

		# if self.spread > 1: 
		# 	self.position_index = float((sum([leaf_range.index(leaf) for leaf in self.init_const])/self.spread))
		# else:
		# 	demands = [demand for demand in sorted(self.init_const.itervalues())]
		# 	max_demand = max(demands)
		# 	self.position_index = max(self.init_const, key=stats.get)

		#print "position:%i, position_index: %i | %i, init_const: %s" % (self.leaf_range[int(self.position_index)], self.position_index, int(self.position_index), self.init_const)

		#print "Position %f : spread %i" % (self.position_index, self.spread)

	def iterate(self, delta_t):
		raise NotImplementedError

class app_gen_mobile(app_gen):

	def __init__(self, init_const, demand_cap, leaf_range, rate, boundary_bounce, move_after):
		app_gen.__init__(self, init_const, demand_cap, leaf_range, boundary_bounce, move_after)
		self.rate = rate

		# Characteristics
		self.direction = float(rnd.choice([-1,1]))

	def iterate(self, delta_t):
		#print "Rate: %f - %f ->" % (self.rate, self.position_index) ,
		self.position_index += float(self.direction*self.rate)
		#print " %f ->" % self.position_index,

		# Check boundary conditions
		if self.boundary_bounce:
			if self.position_index > self.max_leaf_index:
				self.direction = self.direction*(-1.0)
				self.position_index = float(self.max_leaf_index)
			elif self.position_index < self.min_leaf_index:
				self.direction = self.direction*(-1.0)
				self.position_index = float(self.min_leaf_index)

			eval_range = range(self.nbr_leafs)

			assert self.position_index <= self.max_leaf_index and self.position_index >= self.min_leaf_index, 'Mobile - Position out of bounds %f' % self.position_index
		else : 
			self.position_index = self.position_index % self.nbr_leafs

		#print " %f|%i ->" % (self.position_index, self.leaf_range[int(self.position_index)]),
		# Determine new midpoint

		lower = int(np.ceil(np.floor(self.position_index)-self.spread_per_side))%self.nbr_leafs
		upper = int(np.floor(np.ceil(self.position_index)+self.spread_per_side))%self.nbr_leafs

		eval_range = xrange(lower, upper)
		only_full_range = xrange(min(lower+1,upper), max(lower,upper-1))

		#print " %s ->" % (eval_range),
		#print "position_index: %f, eval_range: %s |%i - %i|" % (self.position_index, eval_range, lower, upper)

		demand_per_leaf = self.total_demand/max(1, len(eval_range)-1)
		decimal = self.position_index - np.floor(self.position_index)

		result = {}

		for leaf_index in eval_range:
			result[self.leaf_range[leaf_index]] = demand_per_leaf

		result[self.leaf_range[lower]] = 0.0
		result[self.leaf_range[upper]] = 0.0

		result[self.leaf_range[lower]] += (1-decimal)*demand_per_leaf
		result[self.leaf_range[upper]] += decimal*demand_per_leaf

		for leaf_id, demand in result.iteritems():
			result[leaf_id] = np.round(demand,2)

		#print " %s" % ({leaf_id:demand for leaf_id, demand in result.iteritems() if demand > 0.0})

		self.iterations +=1

		if self.iterations <= self.move_after:
			return self.init_const
		else:
			return result

class app_gen_stationary(app_gen):

	def __init__(self, init_const, demand_cap, leaf_range, boundary_bounce, move_after):
		app_gen.__init__(self, init_const, demand_cap, leaf_range, boundary_bounce, move_after)

		for leaf_id, demand in self.init_const.iteritems():
			self.init_const[leaf_id] = np.round(demand,2)

	def iterate(self, delta_t):
		result = dict([(leaf, 0) for leaf in self.leaf_range])
		resul = result.update(self.init_const)

		total_demand = sum([quantity for quantity in result.itervalues()])
		assert total_demand > 0, 'Stationary - Demand zero'
		#assert total_demand-self.demand_cap == 0.0, 'Stationary - Not equal to alotted demand: %f vs. %f' % (total_demand, self.demand_cap)

		return result

class app_gen_diurnal(app_gen):

	def __init__(self, init_const, demand_cap, leaf_range, sigma_range, cycle, phase_offset, boundary_bounce, move_after):
		app_gen.__init__(self, init_const, demand_cap, leaf_range, boundary_bounce,2)
		self.sigma_range = linspace(sigma_range[0], sigma_range[-1], cycle)
		
		# Characteristics
		self.sigma_index = phase_offset

	def iterate(self, t):

		mid_point = int(self.position_index)
		if self.boundary_bounce:
			eval_range = xrange(max(mid_point-self.nbr_leafs/2, 0), min(mid_point+self.nbr_leafs/2, self.nbr_leafs-1))
		else: 
			eval_range = xrange((mid_point-self.nbr_leafs/2)%self.nbr_leafs, (mid_point+self.nbr_leafs/2)%self.nbr_leafs)


		indexed_result = dict([(leaf, norm.pdf(leaf, self.position_index, self.sigma_range[self.sigma_index])*self.demand_cap) for leaf in eval_range])
		total_demand = sum([quantity for quantity in indexed_result.itervalues()])

		result = {}

		for leaf, quantity in indexed_result.iteritems():
			result[self.leaf_range[leaf]] = np.round(quantity/total_demand*self.demand_cap, 2)

		self.sigma_index =(-1)**int(t/(len(self.sigma_range)-1))

		total_demand = sum([quantity for quantity in result.itervalues()])
		assert total_demand > 0, 'Diurnal - Demand zero'
		#assert total_demand-self.demand_cap <= 0.0, 'Diurnal - Not equal to alotted demand: %f vs. %f' % (total_demand, self.demand_cap)

		self.iterations +=1

		if self.iterations <= self.move_after:
			return self.init_const
		else:
			return result

'''
Workload generator
'''
class workload_generator(object):
	def __init__(self, network, leafs, roots, nbr_apps, file_path = "workloads/verification_workload"):
		self.network = network
		self.leafs = leafs
		self.roots = roots
		self.nbr_apps = nbr_apps
		self.file_path = file_path

		self.dcs = sorted(self.network.node)
		self.nbr_nodes = len(self.dcs)
		self.nbr_leafs = len(self.leafs)

		self.move_after = 16.0

	def write_to_file(self, workload):
		# Convert python dictionary to JSON file
		data_json = json.dumps(workload, sort_keys=True, indent=2)

		# Wite JSON structure to file
		f = open(self.file_path, 'w')
		f.write(data_json)
		f.close()

	def app_type_factory(self, app_type, init_const, demand_cap, leaf_range, epoch, boundary_bounce):
		if app_type == 'STATIONARY':
			return app_gen_stationary(init_const, demand_cap, leaf_range, boundary_bounce, 11) 
		elif app_type == 'MOBILE': #init_const, demand_cap, leaf_range, rate, boundary_bounce
			return app_gen_mobile(init_const=init_const, demand_cap=demand_cap, leaf_range=leaf_range, rate=(0.01 + np.random.sample()*0.1), boundary_bounce=boundary_bounce, move_after=self.move_after)
		elif app_type == 'DIURNAL':
			return app_gen_diurnal(init_const, demand_cap, leaf_range, sigma_range=[rnd.choice([1.0, 1.75]), rnd.choice([2.0, 5.0])], cycle=50, phase_offset=int(rnd.random()*epoch), boundary_bounce=boundary_bounce, move_after=self.move_after)
		else:
			print 'Unknown application demand type %s' % app_type

	def generate(self, load_per_type, scale_per_type, global_scale=1.0, duration=10, boundary_bounce=True):
		raise NotImplementedError("Subclasses should implement this!")

	def determine_node_depths(self):
		'''
		Determine how deep each node is from root.
		'''
		result = {}
		for node in self.dcs:
			depth = min( [len(nx.shortest_path(G=self.network, source=node, target=root) ) -1 for root in self.roots] )
			result[node] = depth

		return result

	def determine_type_depths(self, node_depth_map, load_per_type):
		'''
		Type = {Small, Medium, Large, Huge}
		Determine how deep each type is.
		'''
		result = {}
		for dc_type in range(len(load_per_type)):
			result[dc_type] = sys.maxint

		#for node_id, depth in node_depth_map.iteritems():
		for node_id in self.dcs:
			depth = min([len(nx.shortest_path(G=self.network, source=node_id, target=self.leafs[i])) for i in range(self.nbr_leafs)])
			dc_type = self.network.node[node_id]['type']
			current_value = result[dc_type]
			result[dc_type] = min(depth, current_value)

		return result

	def map_leafs_to_node(self, type_depth_map):
		result = {}
		for node in self.dcs:
			dists = [len(nx.shortest_path(G=self.network, source=node, target=self.leafs[i])) for i in range(self.nbr_leafs)]
			min_dist = min(dists)

			dc_type = self.network.node[node]['type']

			if dc_type == 3:
				factor = 1
			else:
				factor = 0	
			result[node] = [i for i in range(self.nbr_leafs) if dists[i] <= type_depth_map[dc_type]]

		return result

	def allocate_apps(self, node_depth_map, node_leafs_map, load_per_type, scale_per_type, global_scale, duration, boundary_bounce):
		# Allocate number of apps
		apps_per_node = int(np.floor(float(self.nbr_apps)/float(self.nbr_nodes)))
		remainder = self.nbr_apps - apps_per_node*self.nbr_nodes

		app_dist = {node_id:apps_per_node for node_id in range(self.nbr_nodes)} # Allocate number of apps evenly

		apps = {}

		for node_id in np.random.choice(range(self.nbr_nodes), remainder):
			app_dist[node_id]+=1

		app_index = 0
		for node_id, node_nbr_apps in app_dist.iteritems():
			dc_type = self.network.node[node_id]['type']

			# nbr apps for each node
			for i in range(node_nbr_apps):
				demand_nbr_leafs = float( len(node_leafs_map[node_id]) )
				# Dict with demand for app
				app_demand = {self.leafs[j]:(load_per_type[dc_type]*scale_per_type[dc_type]*global_scale/float(app_dist[node_id]))/demand_nbr_leafs for j in node_leafs_map[node_id]}

				apps[app_index] = self.app_type_factory( rnd.choice(['STATIONARY', 'MOBILE', 'DIURNAL'], p=self.app_type_dist[node_depth_map[node_id]]), 
					app_demand, 
					load_per_type[dc_type]*scale_per_type[dc_type]*global_scale/float(app_dist[node_id]), 
					self.leafs,
					duration,
					boundary_bounce
					)

				app_index +=1

		return apps

	def initialise_apps(self, load_per_type, scale_per_type, global_scale, duration, boundary_bounce):
		node_depth_map = self.determine_node_depths()
		type_depth_map = self.determine_type_depths(node_depth_map, load_per_type)
		node_leafs_map = self.map_leafs_to_node(type_depth_map)
		apps = self.allocate_apps(node_depth_map, node_leafs_map, load_per_type, scale_per_type, global_scale, duration, boundary_bounce)

		return apps, type_depth_map, node_leafs_map

class workload_generator_mobile(workload_generator):
	def __init__(self, network, leafs, roots, nbr_apps, file_path = "workloads/verification_workload"):
		workload_generator.__init__(self, network, leafs, roots, nbr_apps, file_path)

		# [STATIONARY, MOBILE, DIURNAL]
		self.app_type_dist = [ 	
							[1.0, 0.0, 0.0], # Huge
							[0.6, 0.4, 0.0], # Large
							[0.8, 0.2, 0.0], # Medium
							[1.0, 0.0, 0.0]  # Small
						]

	def generate(self, load_per_type, scale_per_type, global_scale=1.0, duration=10, boundary_bounce=True):
		apps, type_depth_map, node_leafs_map = self.initialise_apps(load_per_type, scale_per_type, global_scale, duration, boundary_bounce)

		self.file_path = self.file_path + "_a_%i_t_%i_d_%i_MOBILE.json" % (self.nbr_apps, duration, global_scale)

		workload = {}

		''' Iterate the generator '''
		for t in range(duration):
			workload[t] = {}
			for app_name, generator in apps.iteritems():
				workload[t][app_name] = generator.iterate(t)

		# ''' Calculate load distribution '''
		# for leaf in leafs:
		# 	print "\t%s" % leaf,
		# print " | Total demand:"
		# print "\t",
		# for leaf in leafs:
		# 	print "---",
		# print ""

		''' Calculate total demand '''
		for t, data in sorted(workload.iteritems()):
			leaf_dist = {leaf:0.0 for leaf in self.leafs}
			total = 0.0
			for demand_dist in data.itervalues():
				for leaf, demand in demand_dist.iteritems():
					leaf_dist[leaf] += demand
					total +=demand 

			# for demand in leaf_dist.itervalues():
			# 	print "\t%i" % int(demand/total*100),
			# print " | %i" % int(total)

		# Wite JSON stucture to file

		# f = open(file_path, 'w')
		# f.write(data_json)
		# f.close()

		sla_per_type = {}

		print "type_depth_map %s" % type_depth_map

		sla_per_type[1] = (type_depth_map[0], type_depth_map[1])
		sla_per_type[2] = (type_depth_map[1], type_depth_map[2])
		sla_per_type[3] = (type_depth_map[2], type_depth_map[3])

		demand_dist_node = [len(spread) for spread in node_leafs_map.itervalues()]

		result = {}
		result['WORKLOAD'] = workload
		result['STATS'] = {"MAX_APP_SPREAD":demand_dist_node, "SLA_PER_TYPE":sla_per_type}

		self.write_to_file(result)

		return True, self.file_path

class workload_generator_static(workload_generator):
	def __init__(self, network, leafs, roots, nbr_apps, file_path = "workloads/verification_workload"):
		workload_generator.__init__(self, network, leafs, roots, nbr_apps, file_path)

		# [STATIONARY, MOBILE, DIURNAL]
		self.app_type_dist = [ 	
							[1.0, 0.0, 0.0], # Huge
							[1.0, 0.0, 0.0], # Large
							[1.0, 0.0, 0.0], # Medium
							[1.0, 0.0, 0.0]  # Small
						]

	def generate(self, load_per_type, scale_per_type, global_scale=1.0, duration=10, boundary_bounce=True):
		apps, type_depth_map, node_leafs_map = self.initialise_apps(load_per_type, scale_per_type, global_scale, duration, boundary_bounce)

		self.file_path = self.file_path + "_a_%i_t_%i_d_%i_STATIC.json" % (self.nbr_apps, duration, global_scale)

		workload = {}

		''' Iterate the generator '''
		for t in range(duration):
			workload[t] = {}
			for app_name, generator in apps.iteritems():
				workload[t][app_name] = generator.iterate(t)

		# ''' Calculate load distribution '''
		# for leaf in leafs:
		# 	print "\t%s" % leaf,
		# print " | Total demand:"
		# print "\t",
		# for leaf in leafs:
		# 	print "---",
		# print ""

		''' Calculate total demand '''
		for t, data in sorted(workload.iteritems()):
			leaf_dist = {leaf:0.0 for leaf in self.leafs}
			total = 0.0
			for demand_dist in data.itervalues():
				for leaf, demand in demand_dist.iteritems():
					leaf_dist[leaf] += demand
					total +=demand 

			# for demand in leaf_dist.itervalues():
			# 	print "\t%i" % int(demand/total*100),
			# print " | %i" % int(total)

		sla_per_type = {}

		print "type_depth_map %s" % type_depth_map

		sla_per_type[1] = (type_depth_map[0], type_depth_map[1])
		sla_per_type[2] = (type_depth_map[1], type_depth_map[2])
		sla_per_type[3] = (type_depth_map[2], type_depth_map[3])

		# sla_per_type[1] = (type_depth_map[0], type_depth_map[0]+0.5)
		# sla_per_type[2] = (type_depth_map[2], type_depth_map[2]+0.25)
		# sla_per_type[3] = (type_depth_map[3], type_depth_map[3]+0.125)

		demand_dist_node = [len(spread) for spread in node_leafs_map.itervalues()]

		result = {}
		result['WORKLOAD'] = workload
		result['STATS'] = {"MAX_APP_SPREAD":demand_dist_node, "SLA_PER_TYPE":sla_per_type}

		self.write_to_file(result)

		return True, self.file_path

# def generate(network, leafs, roots, nbr_apps, load_per_type, scale_per_type, global_scale=1.0, duration=10, file_path = "workloads/verification_workload", boundary_bounce=True):
# 	# 				STATIONARY, MOBILE, DIURNAL
# 	app_type_dist = [ 	[1.0, 0.0, 0.0], # Huge
# 						[0.6, 0.4, 0.0], # Large
# 						[0.8, 0.2, 0.0], # Medium
# 						[1.0, 0.0, 0.0]  # Small
# 					]

# 	apps = {}

# 	dcs = sorted(network.node)
# 	nbr_nodes = len(dcs)
# 	nbr_leafs = len(leafs)

# 	# System
# 	depth_per_type = {}
# 	for i in range(len(load_per_type)):
# 		depth_per_type[i] = []

# 	''' OLD '''
# 	node_depth_map = {}
# 	for node in dcs:
# 		depth = min( [len(nx.shortest_path(G=network, source=node, target=root) ) -1 for root in roots] )
# 		node_depth_map[node] = depth

# 	''' NEW '''
# 	for node in dcs:
# 		dc_type = network.node[node]['type']
# 		dists = [len(nx.shortest_path(G=network, source=node, target=leafs[i])) for i in range(len(leafs))]
# 		min_dist = min(dists)
	
# 		depth_per_type[dc_type].append(min_dist)

# 	depths = [depth for depth in node_depth_map.itervalues()]
# 	max_depth = max(depths)

# 	# Determine the spread of demand for each node

# 	''' OLD '''
# 	# serviced_leafs = {}
# 	# for node in dcs:
# 	# 	dists = [len(nx.shortest_path(G=network, source=node, target=leafs[i])) for i in range(len(leafs))]
# 	# 	min_dist = min(dists)

# 	# 	dc_type = network.node[node]['type']

# 	# 	if dc_type == 3:
# 	# 		factor = 1
# 	# 	else:
# 	# 		factor = 0	
# 	# 	serviced_leafs[node] = [i for i in range(len(leafs)) if dists[i] <= min_dist+factor]

# 	''' NEW '''
# 	serviced_leafs = {}
# 	for node in dcs:
# 		dists = [len(nx.shortest_path(G=network, source=node, target=leafs[i])) for i in range(len(leafs))]
# 		min_dist = min(dists)

# 		dc_type = network.node[node]['type']

# 		if dc_type == 3:
# 			factor = 1
# 		else:
# 			factor = 0	
# 		serviced_leafs[node] = [i for i in range(len(leafs)) if dists[i] <= min(depth_per_type[dc_type])]

# 	demand_dist_node = [len(spread) for spread in serviced_leafs.itervalues()]

# 	# Allocate number of apps
# 	apps_per_node = int(np.floor(float(nbr_apps)/float(nbr_nodes)))
# 	app_dist = {node_id:apps_per_node for node_id in range(nbr_nodes)} # Allocate number of apps evenly
# 	remainder = nbr_apps - apps_per_node*nbr_nodes

# 	for node_id in np.random.choice(range(nbr_nodes), remainder):
# 		app_dist[node_id]+=1

# 	app_index = 0
# 	for node_id, node_nbr_apps in app_dist.iteritems():
# 		dc_type = network.node[node_id]['type']

# 		# nbr apps for each node
# 		for i in range(node_nbr_apps):
# 			demand_nbr_leafs = float( len(serviced_leafs[node_id]) )
# 			# Dict with demand for app
# 			app_demand = {leafs[j]:(load_per_type[dc_type]*scale_per_type[dc_type]*global_scale/float(app_dist[node_id]))/demand_nbr_leafs for j in serviced_leafs[node_id]}

# 			apps[app_index] = app_type_factory( 
# 				rnd.choice(['STATIONARY', 'MOBILE', 'DIURNAL'], p=app_type_dist[node_depth_map[node_id]]), 
# 				app_demand, 
# 				load_per_type[dc_type]*scale_per_type[dc_type]*global_scale/float(app_dist[node_id]), 
# 				leafs, 
# 				duration,
# 				boundary_bounce
# 				)

# 			app_index +=1

# 	workload = {}

# 	for t in range(duration):
# 		workload[t] = {}
# 		for app_name, generator in apps.iteritems():
# 			workload[t][app_name] = generator.iterate(t)

# 	# ''' Calculate load distribution '''
# 	# for leaf in leafs:
# 	# 	print "\t%s" % leaf,
# 	# print " | Total demand:"
# 	# print "\t",
# 	# for leaf in leafs:
# 	# 	print "---",
# 	# print ""

# 	''' Calculate total demand '''
# 	for t, data in sorted(workload.iteritems()):
# 		leaf_dist = {leaf:0.0 for leaf in leafs}
# 		total = 0.0
# 		for demand_dist in data.itervalues():
# 			for leaf, demand in demand_dist.iteritems():
# 				leaf_dist[leaf] += demand
# 				total +=demand 

# 		# for demand in leaf_dist.itervalues():
# 		# 	print "\t%i" % int(demand/total*100),
# 		# print " | %i" % int(total)

# 	file_path = file_path + "_a_%i_t_%i_d_%i_mobile_hetero.json" % (len(apps), duration, global_scale)

# 	# Convert pyhton dictionary to JSON file
# 	data_json = json.dumps(workload, sort_keys=True, indent=2)

# 	# Wite JSON stucture to file
# 	f = open(file_path, 'w')
# 	f.write(data_json)
# 	f.close()

# 	sla_per_type = {}
# 	sla_per_type[1] = (min(depth_per_type[0]), max(depth_per_type[0])+0.5)
# 	sla_per_type[2] = (min(depth_per_type[2]), max(depth_per_type[2])+0.25)
# 	sla_per_type[3] = (min(depth_per_type[3]), max(depth_per_type[3])+0.125)

# 	return (True, file_path, demand_dist_node, sla_per_type)