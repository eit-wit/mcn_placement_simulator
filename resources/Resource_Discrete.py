import logging 
import random as rnd
import pulp
import math
import networkx as nx

from resources.Resource import Resource

class Resource_Discrete(Resource):
	
	def __init__(self, name, resources, unit_capacities, budget_factor, epoch, expulsion_strategy):
		self.unit_capacities = unit_capacities

		Resource.__init__(self, name, resources, budget_factor, epoch, expulsion_strategy)

		# Discrete resource allocation
		self.allocated_vms = 0.0
		self.resource_utilisation = {}

	'''
	Cost - Discrete
	'''
	# Calculate usage and allocate resources
	def calculate_usage(self):
		resource_utilisation = {}
		allocated_vms = 0.0

		# Initialise data structure
		for resource_name in self.resources:
			resource_utilisation[resource_name] = 0.0

		# Accumulation
		for resources in self.customers.itervalues():
			for resource_name, usage in resources.iteritems():
				resource_utilisation[resource_name] += usage

			allocated_vms += self.evaluate_vm_allocation(resources)
		return allocated_vms, resource_utilisation

	def calculate_cost_for_usage(self, usage, delta_time):
		allocated_vms = self.evaluate_vm_allocation(usage)
		return allocated_vms*self.unit_cost*delta_time

	def calculate_cost(self, delta_time):
		self.allocated_vms, self.resource_utilisation = self.calculate_usage()
		self.momentary_cost = self.allocated_vms*self.unit_cost # All sub-routine outputs should be concatenated, not assigned in sub-routines
		return self.momentary_cost

	def interpolate_cost(self, epoch_remainder):
		allocated_vms, total_usage = self.calculate_usage() 
		return allocated_vms*self.unit_cost*epoch_remainder

	def evaluate_vm_allocation(self, demand):
		return math.ceil(max([resource_usage/self.unit_capacities[resource_name] 
					for resource_name, resource_usage in demand.iteritems()]) 
		)

	def evaluate_placement_cost(self, demand):
		return self.evaluate_vm_allocation(demand)*self.unit_cost

	'''
	Behaviour
	'''
	def evaluate_budget(self, time, network, applications):
		self.budget_vilolated = False
		
		# Calculate net-flow
		self.nbr_customers_flow.eval_state(dict([(app_name,1) for app_name in self.customers]))
		self.util_flow.eval_state(
			{app_name:
			self.evaluate_vm_allocation(usage_resource)/self.capacity*100 
			for app_name, usage_resource in self.customers.iteritems()
			}
		)

		# Time management
		delta_time = time - self.prev_time
		self.prev_time = time
		self.elapsed_time += delta_time

		# Compound cost
		self.compound_cost(delta_time)

		# # Check resource utilisation
		# if self.allocated_vms > self.capacity:
		# 	return self.select_expulsion(network, applications, time)
		# 	print "%s: %s resource usage exceeded - %f %%" % (self.name, resource_name, resource_usage/self.resources[resource_name]['CAPACITY']*100)

		# Evaluate budget
		# If within epoch
		# if self.name == 0:
		# 	print " [%f] - Allocation: %f, Budget: %f" % (self.elapsed_time, self.allocated_vms/self.capacity, self.accumulated_cost/self.budget)

		''' Estimated cost '''
		self.est_budget_surplus = self.budget - self.accumulated_cost - self.interpolate_cost(self.epoch - self.elapsed_time)

		''' Evaluate budget'''
		if self.elapsed_time <= self.epoch and self.budget - self.accumulated_cost <= 0.0 :
			if self.time_of_violation == None:
				self.time_of_violation = self.elapsed_time

			self.budget_surplus = 0.0
			self.epoch_deficit = self.interpolate_cost(self.epoch-self.time_of_violation)

			self.budget_vilolated = True
			
			# if self.name == 0:
			# 	print " [%f] - Budget violated" % (self.elapsed_time)

			return self.select_expulsion(network, applications, time)

		# If epoch expired
		elif self.elapsed_time >= self.epoch:
			self.budget_surplus = self.budget - self.accumulated_cost

			self.renew_epoch()

		# Return no apps if noting is violated
		return []

	def expulsion_complete(self):
		self.renew_epoch()

	def renew_epoch(self):
		# if self.name == 0:
		# 	print " [%f] - Epoch renewed" % (self.elapsed_time)

		self.time_of_violation = None
		self.elapsed_time = 0.0
		self.accumulated_cost = 0.0

	'''
	Selection
	'''
	def select_expulsion(self, network, applications, time):
		def calculate_migration_cost(network, neighbour, app_name):
			if neighbour != self.name:
				return network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='MIGRATION'))
			else:
				return 0.0

		def get_links(network, source, target): # [TO-DO] add path cache
			links = []

			path = nx.shortest_path(G=network, source=source, target=target)
			for (n1,n2) in zip(path[0:],path[1:]):
				links.append(network.edge[n1][n2]['object'])

			return links

		def calculate_link_costs(network, application, target, delta_time=self.epoch):
			cost = 0.0
			for location, quantity in application.get_resource_usage_per_leaf('NET').iteritems():
				for link in get_links(network, self.name, int(location)):
					cost += link.calculate_cost_for_usage({'NET':quantity},delta_time)

			return cost

		def calculate_link_costs_prop(network, application, target, delta_time=self.epoch):
			cost = 0.0
			for location, quantity in application.get_resource_usage_per_leaf('NET').iteritems():
				if len(link) == 0:
					print "(App%i, DC%i) No links" % (int(application.name),int(target))
					cost += 1.0
				for link in get_links(network, self.name, int(location)):
					cost += link.calculate_cost_for_usage({'NET':quantity},delta_time)# / link.budget

			#print cost
			return cost

		# Reset mitigation outcome
		self.mitigation_failed = False

		assert self.expulsion_strategy in Resource.EXPULSION_STRATEGIES, '%s is not a valid expulsion strategy' % self.expulsion_strategy 

		result = []

		''' Aggregating neighbours '''
		neighbourhood = [neighbour for neighbour in network.neighbors(self.name)+[self.name] if network.node[neighbour]['object'].get_budget_surplus()>0]
		if len(neighbourhood) == 0:
			self.mitigation_failed = True
			return result

		if self.expulsion_strategy in Resource.STRATEGIES_INCLUDE_SELF:
			neighbourhood.append(self.name)

		''' Aggregating applications '''
		resident_apps = [app_name for app_name in self.customers if not applications[app_name].migrating()]

		if len(resident_apps) == 0:
			self.mitigation_failed = True
			return result

		'''
		Expulsion 
		'''
		# Random
		if self.expulsion_strategy == 'RANDOM':
			choices = [app_name for app_name in resident_apps if applications[app_name].migrating() == 0]
			if len(choices) > 0: 
				result = [rnd.choice([app_name for app_name in resident_apps if applications[app_name].migrating() == 0])]
			else:
				self.mitigation_failed = True

		# None
		elif self.expulsion_strategy == 'NONE':
			print " - %s - Viloated with no mitigation scheme: Budget surplus: %f, allocation: %f %%" % (self.name, self.budget_surplus, self.allocated_vms/self.capacity)

		# Heaviest
		elif self.expulsion_strategy == 'HEAVIEST': 
			cost = {}

			# Decision variables
			assign_vars = pulp.LpVariable.dicts("AppSelection", [app_name for app_name in resident_apps], 0, 1, pulp.LpBinary)

			# Problem
			prob = pulp.LpProblem("HeaviestAppSelection", pulp.LpMaximize)

			# Objective
			for app_name in resident_apps:
				usage_resource = self.customers[app_name]
				prob += pulp.lpSum(assign_vars[app_name]*sum([ usage * self.resources[resource_name]['COST'] for resource_name, usage in usage_resource.iteritems() ]))

			# Not migrating 
			for app_name in resident_apps:
				prob += assign_vars[app_name]*applications[app_name].migrating() == 0, ''

			# There can be only one
			prob += pulp.lpSum([assign_vars[app_name] for app_name in resident_apps]) == self.max_nbr_apps, ''

			# Solve the problem
			prob.solve()

			# Extract result
			TOL = 0.00001
			result = [app_name for app_name in resident_apps if assign_vars[app_name].varValue > TOL]

		elif self.expulsion_strategy in Resource.STRATEGIES_SOLVER: 
			''' Common init '''

			# Decision variables
			assign_vars = pulp.LpVariable.dicts("NeighbourSelection", [(app_name, neighbour) for app_name in resident_apps for neighbour in neighbourhood], 0, 1, pulp.LpBinary)

			''' Method specifics '''
			if self.expulsion_strategy == 'MAX_REMAINING':
				# Problem
				prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

				# Objective
				prob += pulp.lpSum( ( assign_vars[(app_name, neighbour)] * 
					(network.node[neighbour]['object'].get_budget_surplus() 
							- ( network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))
								+ calculate_migration_cost(network, neighbour, app_name)
							)
						)
					for neighbour in neighbourhood for app_name in resident_apps ) ) 

				# Number of selections
				prob += pulp.lpSum(assign_vars[(app_name, neighbour)] for neighbour in neighbourhood for app_name in resident_apps) <= self.max_nbr_apps, ''

			elif self.expulsion_strategy == 'MAX_SURPLUS':
				# Problem
				prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

				# Objective
				prob += pulp.lpSum(  assign_vars[(app_name, neighbour)] * 
					network.node[neighbour]['object'].get_budget_surplus_percent() 
					for neighbour in neighbourhood for app_name in resident_apps ) 

				# Number of selections
				prob += pulp.lpSum(assign_vars[(app_name, neighbour)] for neighbour in neighbourhood for app_name in resident_apps) <= self.max_nbr_apps, ''

			elif self.expulsion_strategy == 'LOCAL_BEST_EFFORT_EXP':
				app_link_cost_registry = {(neighbour, app_name): max( calculate_link_costs(network=network, application=applications[app_name], target=neighbour), 1) for neighbour in neighbourhood for app_name in resident_apps}
				max_app_link_cost = max([cost for cost in app_link_cost_registry.itervalues()])

				for (neighbour, app_name), cost in app_link_cost_registry.iteritems():
					if cost <= 0:
						print " ---- Link cost for App%i in DC%i is zero : max(%f, %f)/%f" % (int(app_name), int(neighbour), app_link_cost_registry[(neighbour,app_name)],0.2*max_app_link_cost, max_app_link_cost)
					if (max(app_link_cost_registry[(neighbour,app_name)],0.2*max_app_link_cost)/max_app_link_cost) <= 0:
						print " ---- Link cost for App%i in DC%i is zero %f / %f : " (int(app_name), int(neighbour), app_link_cost_registry[(neighbour,app_name)], max_app_link_cost)

				budget_surpluses = dict([(neighbour,network.node[neighbour]['object'].get_budget_surplus()) for neighbour in neighbourhood])
				budget_surpluses[self.name] = self.budget

				# Problem
				prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

				# Objective
				prob += pulp.lpSum(
					assign_vars[(app_name, neighbour)] * 
					math.exp( 
						(1.0)*
						(1.0 - ( network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))*self.epoch
								+ calculate_migration_cost(network, neighbour, app_name)
							)
						/ budget_surpluses[neighbour]
						)
					)
					* 
					math.exp( 
						(1.0)*
						(
						applications[app_name].evaluate_sla_95th_deficite_target_percent(network=network, target=neighbour)
						)
					)
					*
					math.exp((1.0)*(
						1.0-app_link_cost_registry[(neighbour,app_name)]/app_link_cost_registry[(self.name,app_name)]
						))
					for app_name in resident_apps for neighbour in neighbourhood
					)

			elif self.expulsion_strategy == 'LOCAL_BEST_EFFORT':
				app_link_cost_registry = {(neighbour, app_name): max( calculate_link_costs(network=network, application=applications[app_name], target=neighbour), 1) for neighbour in neighbourhood for app_name in resident_apps}
				max_app_link_cost = max([cost for cost in app_link_cost_registry.itervalues()])

				for (neighbour, app_name), cost in app_link_cost_registry.iteritems():
					if cost <= 0:
						print " ---- Link cost for App%i in DC%i is zero : max(%f, %f)/%f" % (int(app_name), int(neighbour), app_link_cost_registry[(neighbour,app_name)],0.2*max_app_link_cost, max_app_link_cost)
					if (max(app_link_cost_registry[(neighbour,app_name)],0.2*max_app_link_cost)/max_app_link_cost) <= 0:
						print " ---- Link cost for App%i in DC%i is zero %f / %f : " (int(app_name), int(neighbour), app_link_cost_registry[(neighbour,app_name)], max_app_link_cost)

				budget_surpluses = dict([(neighbour,network.node[neighbour]['object'].get_budget_surplus()) for neighbour in neighbourhood])
				budget_surpluses[self.name] = self.budget

				# Problem
				prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

				# Objective
				prob += pulp.lpSum(
					assign_vars[(app_name, neighbour)] * 
					math.exp( 
						(1.0)*
						(1.0 - ( network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))*self.epoch
								+ calculate_migration_cost(network, neighbour, app_name)
							)
						/ budget_surpluses[neighbour]
						)
					)
					* 
					math.exp( 
						(1.0)*
						(
						applications[app_name].evaluate_sla_95th_surplus_target_percent(network=network, target=neighbour)
						)
					)
					*
					math.exp((1.0)*(
						1.0-app_link_cost_registry[(neighbour,app_name)]/app_link_cost_registry[(self.name,app_name)]
						))
					for app_name in resident_apps for neighbour in neighbourhood
					)

			elif self.expulsion_strategy == 'PROPORTIONAL_RESPONSE':
				margin = 0.5

				# Problem
				prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

				# Objective
				prob += pulp.lpSum(  assign_vars[(app_name, neighbour)] * 
					(network.node[neighbour]['object'].get_budget_surplus() 
							- ( network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))
								+ calculate_migration_cost(network, neighbour, app_name)
							)
						)
					for neighbour in neighbourhood for app_name in resident_apps ) 

				# Budget ambition
				prob += pulp.lpSum(assign_vars[(app_name, neighbour)]*network.node[self.name]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))
				 for neighbour in neighbourhood for app_name in resident_apps) <= self.epoch_deficit*(1+margin), ''

			elif self.expulsion_strategy == 'MAX_NET_SURPUS':
				budget_surpluses = dict([(neighbour,network.node[neighbour]['object'].get_budget_surplus()) for neighbour in neighbourhood])
				budget_surpluses[self.name] = self.budget

				#print "DC%i: %s" %(self.name, budget_surpluses)

				# Problem
				prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

				# Objective
				prob += pulp.lpSum( assign_vars[(app_name, neighbour)] * 
					(budget_surpluses[neighbour]
							- ( network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))*self.epoch
								+ calculate_migration_cost(network, neighbour, app_name)
							)
						)
					for neighbour in neighbourhood for app_name in resident_apps ) 

			elif self.expulsion_strategy == 'MAX_NET_SURPUS_PERCENT':
				budget_surpluses = dict([(neighbour,network.node[neighbour]['object'].get_budget_surplus()) for neighbour in neighbourhood])
				budget_surpluses[self.name] = self.budget

				#print "DC%i: %s" %(self.name, budget_surpluses)

				# Problem
				prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

				# Objective
				prob += pulp.lpSum( assign_vars[(app_name, neighbour)] * 
					(1.0
							- ( network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))*self.epoch
								+ calculate_migration_cost(network, neighbour, app_name)
							) / budget_surpluses[neighbour]
						)
					for neighbour in neighbourhood for app_name in resident_apps ) 

			elif self.expulsion_strategy == 'MIN_DIFF':
				budget_surpluses = dict([(neighbour,network.node[neighbour]['object'].get_budget_surplus()) for neighbour in neighbourhood])
				budget_surpluses[self.name] = self.budget

				diff_vars = pulp.LpVariable.dicts("Difference", [(neighbour_1, neighbour_2) for neighbour_1 in neighbourhood for neighbour_2 in neighbourhood], 0, 1, pulp.LpBinary)

				# Problem
				prob = pulp.LpProblem("HeaviestAppSelection", pulp.LpMinimize)

				# Objective
				prob += pulp.lpSum(diff_vars)

				for neighbour_1 in neighbourhood:
					for neighbour_2 in neighbourhood:
						prob += diff_vars[(neighbour_1, neighbour_2)] == math.abs( pulp.lpSum( 
							assign_vars[(app_name, neighbour_1)] * 
							( network.node[neighbour_1]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))*self.epoch
								+ calculate_migration_cost(network, neighbour_1, app_name)
							) / budget_surpluses[neighbour_1] for app_name in resident_apps )
						- pulp.lpSum( 
							( network.node[neighbour_2]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))*self.epoch
								+ calculate_migration_cost(network, neighbour_2, app_name)
							) / budget_surpluses[neighbour_2] for app_name in resident_apps ) ), ''


			elif self.expulsion_strategy == 'MAX_EQUAL_NET_SURPUS_PERCENTUAL':
				budget_surpluses = dict([(neighbour,network.node[neighbour]['object'].get_budget_surplus()) for neighbour in neighbourhood])
				budget_surpluses[self.name] = self.budget

				#print "DC%i: %s" %(self.name, budget_surpluses)

				# Problem
				prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMinimize)

				# Objective
				prob += pulp.lpSum( assign_vars[(app_name, neighbour)] * 
						(
							( network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))*self.epoch
								+ calculate_migration_cost(network, neighbour, app_name)
							)/budget_surpluses[neighbour]
						)
					for neighbour in neighbourhood for app_name in resident_apps ) 

				# Equal load
				nbr_neighbours = len(neighbourhood)

				for neighbour in neighbourhood:
					others = list(neighbourhood)
					others.remove(neighbour)
					prob += pulp.lpSum(assign_vars[(app_name, neighbour)] * 
						# Remaining budget neighbour
						(
							(1.0 - network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))*self.epoch
								#+ calculate_migration_cost(network, neighbour, app_name)
							)/budget_surpluses[neighbour]
						) for app_name in resident_apps ) >= pulp.lpSum( assign_vars[(app_name, other_neighbour)] * (0.8)*(1.0/nbr_neighbours) *
						# Remaining budget amongst all othersall other
						(
							(1.0 - network.node[other_neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))*self.epoch
								#+ calculate_migration_cost(network, other_neighbour, app_name)
							)/budget_surpluses[other_neighbour]
						) for app_name in resident_apps for other_neighbour in neighbourhood), ''

			elif self.expulsion_strategy == 'MAX_EQUAL_NET_SURPUS_PERCENTUAL_2':
				# Decision variables
				post_surpluses = pulp.LpVariable.dicts("NeighbourSelection", [neighbour for neighbour in neighbourhood])

				budget_surpluses = dict([(neighbour,network.node[neighbour]['object'].get_budget_surplus()) for neighbour in neighbourhood])
				budget_surpluses[self.name] = self.budget

				# Equal load
				nbr_neighbours = len(neighbourhood)

				# Problem
				prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMinimize)

				prob += pulp.lpSum( pulp.lpSum(post_surpluses[neighbour] for neighbour in neighbourhood) - post_surpluses[neighbour] for neighbour in neighbourhood )

				for neighbour in neighbourhood:
					prob += pulp.lpSum(assign_vars[(app_name, neighbour)] * 
						(
							(
							(1.0 - network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))*self.epoch
								+ calculate_migration_cost(network, neighbour, app_name)
							)/budget_surpluses[neighbour]
							) / nbr_neighbours
						) for app_name in resident_apps ) == post_surpluses[neighbour], ''

			elif self.expulsion_strategy == 'MIN_LOCAL_COST':
				budget_surpluses = dict([(neighbour,network.node[neighbour]['object'].get_budget_surplus()) for neighbour in neighbourhood])
				budget_surpluses[self.name] = self.budget

				# Problem
				prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMinimize)

				# Objective
				prob += pulp.lpSum(  assign_vars[(app_name, neighbour)] * 
					(network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))*self.epoch
								+ calculate_migration_cost(network, neighbour, app_name)
								+ calculate_link_costs(network, applications[app_name], neighbour, self.epoch)
							)
						
					for neighbour in neighbourhood for app_name in resident_apps ) 

			''' Common constraints '''
			# Not in memory
			for neighbour in neighbourhood:
				for app_name in resident_apps:
					for mem_node in applications[app_name].get_placement_mem():
						prob += assign_vars[(app_name, neighbour)] != mem_node, ''

			# One DC per app
			for app_name in resident_apps:
				prob += pulp.lpSum(assign_vars[(app_name, neighbour)] for neighbour in neighbourhood) == 1.0, ''

			# Do not allocate more than capacity
			for neighbour in neighbourhood:
				prob += pulp.lpSum(assign_vars[(app_name, neighbour)] * 
					network.node[neighbour]['object'].evaluate_vm_allocation(applications[app_name].get_resource_usage(type='PRODUCTION')) 
					for app_name in resident_apps) <= network.node[neighbour]['object'].get_capacity(), ''

			''' Solve '''
			prob.solve()

			''' Results '''
			TOL = 0.01
			outcome = [(app_name, neighbour) for neighbour in neighbourhood for app_name in resident_apps if assign_vars[(app_name, neighbour)].varValue > TOL]

			if len(outcome) > 0:
				for (app_name, neighbour) in outcome:
					applications[app_name].set_host(neighbour)
			else:
				self.mitigation_failed = True
				
		self.renew_epoch()			
		return result

	'''
	Metrics
	'''
	def get_mean_utilisation(self):
		return sum([(usage/self.resources[resource_name]['CAPACITY']) for resource_name, usage in self.resource_utilisation.iteritems()] ) / len(self.resource_utilisation) * 100

	def get_max_utilisation(self):
		return max([resource_usage/self.resources[resource_name]['CAPACITY'] for resource_name, resource_usage in self.resource_utilisation.iteritems()]) * 100

	def get_allocation(self):
		return self.allocated_vms/self.capacity * 100

	def get_nbr_allocations(self):
		return self.allocated_vms

	def budget_violated(self):
		return self.budget_surplus <= 0.0

	def set_resources(self, resources):
		self.resources = resources

		self.capacity = math.ceil(max([capacity['CAPACITY']/self.unit_capacities[resource] for resource, capacity in self.resources.iteritems()]))
		self.unit_cost = sum([capacity['COST']*self.unit_capacities[resource] for resource, capacity in self.resources.iteritems()])
		self.max_cost = self.capacity*self.unit_cost
		self.budget = self.max_cost * self.epoch * self.budget_factor
