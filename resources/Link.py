from resources.Resource import Resource

class Link(Resource):
	UTILISATION_LEVEL = 0.3

	TYPES = {
		'SMALL':{	
				'NET': {'CAPACITY':3000.0, 'COST':1.125}
			},
		'MEDIUM':{	
				'NET': {'CAPACITY':5000.0, 'COST':1.0}
			},
		'LARGE':{
				'NET': {'CAPACITY':8000.0, 'COST':0.875}
			}
		}

	def __init__(self, name, resources, budget, epoch, expulsion_strategy):
		Resource.__init__(self, name, resources, budget, epoch, expulsion_strategy)

		self.budget = self.max_cost

	# Latency calculation
	def computeLatency(self): # [TO-DO] Implement actual latency function
		for resourceName in self.properties:
			utilisation = self.resources[resourceName]['USAGE']/self.resources[resourceName]['CAPACITY']
			if utilisation >= 1:
				self.properties[resourceName]['LATENCY'] = float('inf')
			else:
				self.properties[resourceName]['LATENCY'] = self.propagation_latency + 1/(1- utilisation)

	# Get link latency
	def get_latency(self, resourceName):
		return self.properties[resourceName]['LATENCY']
	
	def get_available_bw(self):
		total_usage = self.calculate_usage()
		return max( (self.resources['NET']['CAPACITY'] * Link.UTILISATION_LEVEL - total_usage['NET']), 0) # We have no circuit-breaker for over utilisation

	def select_expulsion(self, network, applications, time):
		return []

	def evaluate_budget(self, time, network, applications):
		delta_time = time - self.prev_time
		self.prev_time = time
		self.elapsed_time += delta_time
		
		self.compound_cost(delta_time)

		self.est_budget_surplus = self.budget - self.interpolate_cost(self.epoch)

		return []
