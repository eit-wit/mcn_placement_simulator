import math
import pulp
import sys
import networkx as nx
import random as rnd
import logging 
import scipy.stats as sp
import math
import numpy as np

class LinearAppResrFunc(object):
	def __init__(self, a, b):
		self.a = a
		self.b = b

	def print_values(self):
		return 'a:',self.a, ' b:', self.b

	def compute_resource_usage(self, demand):
		return self.a+self.b*demand

class ExpAppResrFunc(object):
	def __init__(self, a):
		self.a = a
		
	def compute_resource_usage(self, demand):
		# exponential function
		return self.a+math.exp(demand)
		
class LinearAppMigFuncLimited(object):
	def __init__(self, a):
		self.a = a
		
	def computeMigrationUsage(self, demand, maxAvailable):
		# linear function
		return self.a+math.exp(demand)

# Classes used to calculate migration incurred resource PRODUCTION
class MigrationScheme(object):
	def __init__(self, resources):
		self.resources = resources
		
	def computeMigrationUsage(self, demand, maxAvailable):
		# linear function
		return self.a + math.exp(demand)
		
class Constraint_MigrationScheme(MigrationScheme):
	def __init__(self, resources):
		MigrationScheme.__init__(self, resources)
		
	def computeMigrationUsage(self, maxAvailable):
		# linear function
		return self.a + math.exp(demand)

# SLO/SLA
class Threshold(object):
	def __init__(self, threshold):
		self.threshold = threshold
		
	def compute(self, value):
		if value >= self.threshold:
			return False
		else:
			return True

class Application(object):
	TYPES = {
		'CPU_INTENSIVE':{	
				'CPU': {
					'RESOURCE_FUNCS':{
						'PRODUCTION':LinearAppResrFunc(0.0, 0.95),
						'MIGRATION':LinearAppResrFunc(0.0, 1.1) 
						#'MIGRATION':LinearAppResrFunc(0.0, 0.00) 
						}
						},
				'STORAGE': {
					'RESOURCE_FUNCS':{
						'PRODUCTION':LinearAppResrFunc(0.0, 0.5), 
						'MIGRATION':LinearAppResrFunc(0.0, 0.5)
						#'MIGRATION':LinearAppResrFunc(0.0, 0.00) 
						}
						},
				'NET': {
					'RESOURCE_FUNCS':{
						'PRODUCTION':LinearAppResrFunc(0.0, 0.05), 
						'MIGRATION':LinearAppResrFunc(0.0, 0.05) 
						#'MIGRATION':LinearAppResrFunc(0.0, 0.00) 
						}
						}
			},
		'NET_INTENSIVE':{
				'CPU': {
					'RESOURCE_FUNCS':{
						'PRODUCTION':LinearAppResrFunc(0.0, 0.05), 
						'MIGRATION':LinearAppResrFunc(0.0, 0.55) 
						#'MIGRATION':LinearAppResrFunc(0.0, 0.00) 
						}
						},
				'STORAGE': {
					'RESOURCE_FUNCS':{
						'PRODUCTION':LinearAppResrFunc(0.0, 0.75), 
						'MIGRATION':LinearAppResrFunc(0.0, 1.05)
						#'MIGRATION':LinearAppResrFunc(0.0, 0.00) 
						}
						},
				'NET': {
					'RESOURCE_FUNCS':{
						'PRODUCTION':LinearAppResrFunc(0.0, 0.95), 
						'MIGRATION':LinearAppResrFunc(0.0, 1.05)
						#'MIGRATION':LinearAppResrFunc(0.0, 0.00) 
						}
						}
			},
		'SYMMETRIC':{
				'CPU': {
					'RESOURCE_FUNCS':{
						'PRODUCTION':LinearAppResrFunc(0.0, 0.5), 
						'MIGRATION':LinearAppResrFunc(0.0, 0.5)
						#'MIGRATION':LinearAppResrFunc(0.0, 0.00) 
						}
					},
				'STORAGE': {
					'RESOURCE_FUNCS':{
						'PRODUCTION':LinearAppResrFunc(0.0, 0.75), 
						'MIGRATION':LinearAppResrFunc(0.0, 1.05)
						#'MIGRATION':LinearAppResrFunc(0.0, 0.00) 
						}
						},
				'NET': {
					'RESOURCE_FUNCS':{
						'PRODUCTION':LinearAppResrFunc(0.0, 0.5), 
						'MIGRATION':LinearAppResrFunc(0.0, 0.5) 
						#'MIGRATION':LinearAppResrFunc(0.0, 0.00) 
						}
					}
			}
		# 'RANDOM':{ # TO-DO Create random functions
		# 		'CPU': {
		# 			'RESOURCE_FUNCS':{
		# 				'PRODUCTION':LinearAppResrFunc(0.0, 0.5), 
		# 				'MIGRATION':LinearAppResrFunc(0.0, 1.0) }
		# 			},
		# 		'STORAGE': {
		# 			'RESOURCE_FUNCS':{
		# 				'PRODUCTION':LinearAppResrFunc(0.0, 1.0), 
		# 				'MIGRATION':LinearAppResrFunc(0.0, 1.0) }
		# 				},
		# 		'NET': {
		# 			'RESOURCE_FUNCS':{
		# 				'PRODUCTION':LinearAppResrFunc(0.0, 0.5), 
		# 				'MIGRATION':LinearAppResrFunc(0.0, 1.0) }
		# 			}
		# 	}
		}
	
	RE_EVALUATION_STRATEGIES = [
	'RANDOM', 
	'NONE', 
	'LOCAL_BEST_EFFORT',
	'LOCAL_BEST_EFFORT_EXP',
	'MAX_BUDGET_SURPLUS', 
	'MIN_DISTANCE', 
	'MIN_DEFICITE',
	'MAX_REMAINING_BUDGET',
	'MAX_PEER_SURPLUS',
	'MAX_SLA_SURPLUS'
	]

	def __init__(self, name, properties, sla_range, sla_padding, max_app_spread, placement_mem_size, migration_sla_buffer):
		self.name = name
		self.properties = properties 
		self.sla_range = sla_range
		self.sla = None
		self.max_app_spread = max_app_spread
		self.sla_padding = sla_padding
		self.migration_sla_buffer = migration_sla_buffer

		self.placement_mem = [-sys.maxint - 1 for i in range(placement_mem_size)]
		self.placement_mem_ptr = 0
		self.placement_mem_size = placement_mem_size

		self.demand_type = None # Assigned when demand is updated

		self.demand = {}
		self.paths = {} #Should put this cache in the network
		self.host_incumbent = None
		self.host_elected = None
		self.sla_surplus = None

		self.prev_time = 0.
		self.state_transferred = 0.

		self.migration_remaining = 0.

		self.accumulated_time_sla = 0

		self.migration_failed = False

	def get_placement_mem(self):
		return self.placement_mem
	
	def update_demand(self, demand):

		self.demand = demand

		if self.demand_type == None:
			nbr_demand_nodes = sum([1 for quantity in self.demand.itervalues() if quantity > 0])
			#print "Max spread: %i - Mean spread: %i - spread: %i - Type: " % (max(self.max_app_spread), np.mean(self.max_app_spread), nbr_demand_nodes),

			if nbr_demand_nodes == 1: # Local: one node
				#print "Local",
				self.demand_type = 1
				#print "%s : Local" % self.name
			elif nbr_demand_nodes < max(self.max_app_spread): # More than one less than all
				#print "Regional"
				self.demand_type = 2
				#print "%s : Regional" % self.name
			else:
				#print "Global"
				self.demand_type = 3 # Global: all nodes 
				#print "%s : Global" % self.name

			#print " - SLA: ",

		if self.sla == None:
			sla_lower, sla_upper = self.sla_range[str(self.demand_type)]
			self.sla = sla_lower + np.random.sample()*(sla_upper+self.sla_padding[str(self.demand_type)]-sla_lower)
			#print self.sla

	def propagate_workload(self, network, time):
		def incurr_DC(sinc, usage_type, quantity, network):
			# Incumbent data centre
			for resource_name, properties in self.properties.iteritems():
				network.node[sinc]['object'].subject_demand(
					customer=self.name,
					resource_name=resource_name,
					quantity=properties['RESOURCE_FUNCS'][usage_type].compute_resource_usage(float(quantity))
					)

		def incurr_link(pair, usage_type, quantity, network):
			# Maintain path cache
			if pair not in self.paths:
				(source, sinc) = pair
				path = nx.shortest_path(G=network, source=source, target=sinc)
				self.paths[pair] = zip(path[0:],path[1:])

			for (n1, n2) in self.paths[pair]:
				for resource_name, properties in self.properties.iteritems(): 
					network.edge[n1][n2]['object'].subject_demand(
						customer=self.name, 
						resource_name=resource_name, 
						quantity=properties['RESOURCE_FUNCS'][usage_type].compute_resource_usage(float(quantity))
						)

		def propagate(network, sinc, usage_type):
			# Subject resources to application demand
			for location, quantity in self.demand.iteritems():
				pair = (int(location), sinc)

				# Intermediate links
				incurr_link(pair, usage_type, quantity, network)

				# for (n1, n2) in self.paths[pair]:
				# 	for resource_name, properties in self.properties.iteritems(): 
				# 		network.edge[n1][n2]['object'].subject_demand(
				# 			customer=self.name, 
				# 			resource_name=resource_name, 
				# 			quantity=properties['RESOURCE_FUNCS'][usage_type].compute_resource_usage(float(quantity))
				# 			)
				
				# Sinc
				incurr_DC(sinc, usage_type, quantity, network)

				# for resource_name, properties in self.properties.iteritems():
				# 	network.node[sinc]['object'].subject_demand(
				# 		customer=self.name, 
				# 		resource_name=resource_name, 
				# 		quantity=properties['RESOURCE_FUNCS'][usage_type].compute_resource_usage(float(quantity))
				# 		)

		self.demand = {key: float(value) for key, value in self.demand.items() if float(value) > 0.0}

		assert self.host_incumbent != None , "Application %s has no host" % self.name

		# Served demand
		propagate(network, self.host_incumbent, 'PRODUCTION')

		# Migration demand
		if self.host_elected != None:
			delta_time = time - self.prev_time
			self.prev_time = time

			# Maintain path cache
			pair = (self.host_incumbent, self.host_elected)
			if pair not in self.paths:
				path = nx.shortest_path(G=network, source=self.host_incumbent, target=self.host_elected)
				self.paths[pair] = zip(path[0:],path[1:])

			# Applications total demand
			total_demand = sum([float(q) for q in self.demand.itervalues()])

			# Minimum available bandwidth
			min_bw = min([network.edge[n1][n2]['object'].get_available_bw() for (n1,n2) in self.paths[pair]])

			# Amount to be transferred
			transfer_volume = self.properties['STORAGE']['RESOURCE_FUNCS']['MIGRATION'].compute_resource_usage( total_demand )

			# Check if it is zero
			if transfer_volume > 0:
				self.state_transferred += min_bw * delta_time
			else:
				self.state_transferred = 0.0

			# If migration is ongoing
			if self.state_transferred < transfer_volume:
				# Incur workload on receiving DC
				incurr_DC(self.host_elected, 'MIGRATION', total_demand, network)

				# Incur workload on intermediate links
				for node in self.paths[pair]:
					network.edge[n1][n2]['object'].subject_demand(
						customer=self.name, 
						resource_name='NET', 
						quantity= min_bw * delta_time
						)

				self.migration_remaining = self.state_transferred/transfer_volume*100
				#logging.info('Migration for %s from %i to %i : %f completed' % (self.name, self.host_incumbent, self.host_elected, self.migration_remaining))

			else:
				#logging.info('Migration for %s from %i to %i complete' % (self.name, self.host_incumbent, self.host_elected))
				# Update placement memory
				if self.placement_mem_size > 0:
					self.placement_mem[self.placement_mem_ptr] = self.host_incumbent
					self.placement_mem_ptr = (self.placement_mem_ptr+1)%self.placement_mem_size

				self.host_incumbent = self.host_elected
				self.host_elected = None
				self.state_transferred = 0.0

	# Calculates the resource usage per resource
	def get_resource_usage(self, type='PRODUCTION'):
		assert len(self.demand) > 0, "App #%i - No demand" % (self.name)
		total_demand = sum( [float(demand) for location, demand in self.demand.iteritems()] )
		return self.get_usage(total_demand=total_demand, type=type)

	def get_usage(self, total_demand, type='PRODUCTION'):
		usage_resource = {}
		[usage_resource.update({resource_name: attributed['RESOURCE_FUNCS'][type].compute_resource_usage(total_demand) }) for resource_name, attributed in self.properties.iteritems() ]
		return usage_resource

	def get_usage_for_resource(self, total_demand, resource_name):
		usage_resource = {}
		usage_resource.update({resource_name: self.properties[resource_name]['RESOURCE_FUNCS']['PRODUCTION'].compute_resource_usage(total_demand)})
		return usage_resource

	def get_resource_usage_per_leaf(self, resource_name):
		return {location: self.properties[resource_name]['RESOURCE_FUNCS']['PRODUCTION'].compute_resource_usage(float(demand)) for location, demand in self.demand.iteritems()}

	# Evaluate SLO/SLA
	def evaluate_sla(self, network):
		if not self.migrating():
			self.sla_surplus = self.evaluate_sla_surplus_target(network, self.host_incumbent)
			return self.sla_surplus < self.migration_sla_buffer
		else: 
			return False

	def evaluate_sla_surplus_target_percent(self, network, target):
		return self.evaluate_sla_surplus_target(network, target)/self.sla

	def evaluate_sla_deficite_target(self, network, target):
		result = self.sla - self.evaluate_sla_surplus_target(network, target)
		if result > 0:
			return 0
		else:
			return -1*result

	def evaluate_sla_surplus_target(self, network, target):
		if len(self.demand) > 0:
			path_lengths = []

			for location, quantity in self.demand.iteritems():
				if quantity > 0:
					pair = (int(location), target)

					if pair not in self.paths:
						path = nx.shortest_path(G=network, source=int(location), target=target)
						self.paths[pair] = zip(path[0:],path[1:])

					path_lengths.append(len(self.paths[pair]))

			result = self.sla - max(path_lengths)
		else: 
			result = 0.0
			
		return result
	
	def sla_viloated(self):
		return self.sla_surplus < 0.0

	# Evaluate SLO/SLA
	def evaluate_sla_95th(self, network):
		self.sla_surplus = self.evaluate_sla_95th_surplus_target(network, self.host_incumbent)
		result = self.sla_surplus < self.migration_sla_buffer

		if self.migrating():
			return False
			
		if not result:
			self.accumulated_time_sla += 1

		return result

	def evaluate_sla_95th_surplus_target_percent(self, network, target):
		return self.evaluate_sla_95th_surplus_target(network, target)/self.sla

	def evaluate_sla_95th_deficite_target(self, network, target):
		result = self.sla - self.evaluate_sla_95th_surplus_target(network, target)
		if result > 0:
			return 0
		else:
			return -1*result

	def evaluate_sla_95th_deficite_target_percent(self, network, target):
		result = 1.0 - self.evaluate_sla_95th_surplus_target(network, target)/self.sla
		if result > 0:
			return 0
		else:
			return -1*result

	def evaluate_sla_95th_surplus_target(self, network, target):
		if len(self.demand) > 0:
			quantities = []
			distances = []

			total_demand = sum([float(q) for q in self.demand.itervalues()])

			for location, quantity in self.demand.iteritems():
				if quantity > 0:
					pair = (int(location), target)

					if pair not in self.paths:
						path = nx.shortest_path(G=network, source=int(location), target=target)
						self.paths[pair] = zip(path[0:],path[1:])

					quantities.append(quantity/total_demand)
					distances.append(len(self.paths[pair]))

			mu = sum([quantities[i]*distances[i] for i in range(len(quantities))])
			sigma = math.sqrt(sum([quantities[i]*(distances[i]-mu)**2 for i in range(len(quantities)) ]))

			result = sp.norm.interval(.95, loc=mu, scale=sigma)[1]

			if np.isnan(result):
				return round(self.sla - mu, 4)
			else:
				return round(self.sla - result, 4)

		else: 
			return 0.0
		
	'''
	Metrics
	'''
	def get_accumulated_time_sla(self):
		return self.accumulated_time_sla
		
	def get_sla_surpus_percent(self):
		return self.sla_surplus/self.sla

	def get_sla_deficit(self):
		result = self.sla - self.sla_surplus
		if result > 0:
			return 0
		else:
			return result

	def get_demand(self):
		return self.demand

	def get_sla_surplus(self):
		return self.sla_surplus

	def migrating(self):
		return self.host_elected != None

	def get_placement(self):
		return self.host_incumbent

	def get_placement(self):
		return self.host_incumbent

	def get_demand_type(self):
		return self.demand_type

	def get_mitigation_outcome(self):
		return self.migration_failed

	# Set the host
	def set_host(self, host):
		if self.host_incumbent == None:
			self.host_incumbent = host

		elif host != self.host_incumbent:
			self.host_elected = host
			self.state_transferred = 0.0
			#logging.info('Migrating %s from %i to %i' % (self.name, self.host_incumbent, self.host_elected))

	# Force set host, avoids mirgration
	def force_set_host(self, host):
		self.host_incumbent = host

	# Re evaluate
	def re_evaluate(self, network, strategy):
		def calculate_migration_cost(network, neighbour):
			if neighbour != self.host_incumbent:
				return network.node[neighbour]['object'].evaluate_placement_cost(self.get_resource_usage(type='MIGRATION'))#*5
			else:
				return 0.0

		def get_links(network, source, target): # [TO-DO] add path cache
			links = []
			path = nx.shortest_path(G=network, source=source, target=target)
			for (n1,n2) in zip(path[0:],path[1:]):
				links.append(network.edge[n1][n2]['object'])

			return links

		def get_link_usage_cost(network, target): # Implement get all links
			result = 0.0

			for location, quantity in self.get_resource_usage_per_leaf('NET').iteritems():
				for link in get_links(network, int(location), int(target)):
					result += (link.calculate_cost_for_usage({'NET': quantity},link.epoch))#/link.budget

			#print result
			return result

		assert strategy in Application.RE_EVALUATION_STRATEGIES , "%s is not a valid re evaluation strategy" % strategy
		
		# Extract neighbourhood
		neighbours = network.neighbors(self.host_incumbent)
		if strategy not in ["MIN_DEFICITE", "LOCAL_BEST_EFFORT_EXP", 'MAX_SLA_SURPLUS', 'LOCAL_BEST_EFFORT']: 
			neighbours +=  [self.host_incumbent]
		
		# if strategy == 'MIN_DISTANCE':
		# 	neighbourhood = neighbours
		# else:
		neighbourhood = [neighbour for neighbour in neighbours if network.node[neighbour]['object'].get_budget_surplus()>0]

		# if self.demand_type == 2:
		# 	if len(neighbourhood) == 0:
		# 		print "App:%i -> nowhere to go" % self.name

		# 	if self.migrating():
		# 		print "App:%i -> already migrating" % self.name

		self.migration_failed = False

		if len(neighbourhood) == 0:
			#print "Failed to migrate - no resources available"
			self.migration_failed = True
			return

		if self.migrating():
			self.migration_failed = True
			#print "Failed to migrate - migrating"
			return

		if strategy == 'MAX_PEER_SURPLUS':
			budgets = [network.node[neighbour]['object'].get_budget_surplus() for neighbour in neighbourhood]

			neighbour_index = budgets.index(max(budgets))
			result = neighbourhood[neighbour_index]
			
			self.set_host(result)

		elif strategy == 'MAX_BUDGET_SURPLUS':
			# Decision variables
			assign_vars = pulp.LpVariable.dicts("NeighbourSelection", neighbourhood, 0, 1, pulp.LpBinary)

			# Problem
			prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

			# Objective
			prob += pulp.lpSum( ( assign_vars[neighbour] * 
				( network.node[neighbour]['object'].get_budget_surplus_percent() )
					for neighbour in neighbourhood ) ) 

			# Only one selection
			prob += pulp.lpSum(assign_vars[neighbour] for neighbour in neighbourhood) == 1, ''

			# Not from memory
			for mem_item in self.placement_mem:
				for neighbour in neighbourhood:
					prob += assign_vars[neighbour].name != mem_item, ''

			# Solve it
			prob.solve()

			# Extract result
			TOL = 0.00001
			result = [neighbour for neighbour in neighbourhood if assign_vars[ neighbour ].varValue > TOL][0]

			assert result in neighbourhood , "%i not in %S" % (result, neighbourhood)

			#print "Neighbours to %i:  %s " % (self.host_incumbent, result)
			#network.node[self.host_incumbent]['object'].expulsion_complete()
			self.set_host(result)

		elif strategy == 'NONE':
			pass

		elif strategy == 'MIN_DEFICITE':
			# Decision variables
			assign_vars = pulp.LpVariable.dicts("NeighbourSelection", neighbourhood, 0, 1, pulp.LpBinary)

			# Problem
			prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

			# Objective
			prob += pulp.lpSum(assign_vars[neighbour] * self.evaluate_sla_95th_surplus_target(network=network, target=neighbour) for neighbour in neighbourhood)

			# Only one selection
			prob += pulp.lpSum(assign_vars[neighbour] for neighbour in neighbourhood) == 1, ''

			# Solve it
			prob.solve()

			# Extract result
			TOL = 0.00001
			result = [neighbour for neighbour in neighbourhood if assign_vars[ neighbour ].varValue > TOL][0]

			#network.node[self.host_incumbent]['object'].expulsion_complete()
			self.set_host(result)

		elif strategy == 'MIN_DISTANCE':
			# Decision variables
			assign_vars = pulp.LpVariable.dicts("NeighbourSelection", neighbourhood, 0, 1, pulp.LpBinary)

			# Problem
			prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMinimize)

			# Objective
			prob += pulp.lpSum(assign_vars[neighbour] * max([len(get_links(network=network, source=int(source), target=neighbour)) for source, quantity in self.demand.iteritems() if quantity > 0]) for neighbour in neighbourhood)

			# Only one selection
			prob += pulp.lpSum(assign_vars[neighbour] for neighbour in neighbourhood) == 1, ''

			# Positive budget surplus
			for neighbour in neighbourhood:
				prob += assign_vars[neighbour]*network.node[neighbour]['object'].get_budget_surplus_percent() >= 0.0

			# Solve it
			prob.solve()

			# Extract result
			TOL = 0.00001
			result = [neighbour for neighbour in neighbourhood if assign_vars[ neighbour ].varValue > TOL][0]

			# if result == self.host_incumbent and self.demand_type == 2:
			# 	print "App:%i -> back to myself, %i" % (self.name, len(neighbourhood))

			#network.node[self.host_incumbent]['object'].expulsion_complete()
			self.set_host(result)

		elif strategy == 'MAX_BUDGET_SURPLUS_OLD':
			targets = {}
			[targets.update({neighbour: network.node[neighbour]['object'].get_budget_surplus_percent()}) for neighbour in neighbourhood]
			#print "Neighbours to %i:  %s " % (self.host_incumbent, targets)
			#network.node[self.host_incumbent]['object'].expulsion_complete()
			self.set_host(max(targets, key=targets.get))

		elif strategy == 'MAX_REMAINING_BUDGET':
			# Decision variables
			assign_vars = pulp.LpVariable.dicts("NeighbourSelection", neighbourhood, 0, 1, pulp.LpBinary)

			# Problem
			prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

			# Objective
			prob += pulp.lpSum( ( assign_vars[neighbour] * 
				(network.node[neighbour]['object'].get_budget_surplus() 
						- ( network.node[neighbour]['object'].evaluate_placement_cost(self.get_resource_usage(type='PRODUCTION'))
							+ calculate_migration_cost(network, neighbour)
						)
					)
				for neighbour in neighbourhood ) ) 

			# Not from memory
			for mem_item in self.placement_mem:
				for neighbour in neighbourhood:
					prob += assign_vars[neighbour].name != mem_item, ''

			# Positive budget surplus
			for neighbour in neighbourhood:
				prob += assign_vars[neighbour]*network.node[neighbour]['object'].get_budget_surplus_percent() >= 0.0

			# Only one selection
			prob += pulp.lpSum(assign_vars[neighbour] for neighbour in neighbourhood) == 1, ''

			# Solve it
			prob.solve()

			# Extract result
			TOL = 0.00001
			result = [neighbour for neighbour in neighbourhood if assign_vars[ neighbour ].varValue > TOL][0]

			assert result in neighbourhood , "%i not in %S" % (result, neighbourhood)

			#print "Neighbours to %i:  %s " % (self.host_incumbent, result)
			#network.node[self.host_incumbent]['object'].expulsion_complete()
			self.set_host(result)

		elif strategy == 'MAX_REMAINING_BUDGET_OLD':
			targets = {}
			#print "Neighbours to %i:  %s " % (self.host_incumbent, targets)
			[targets.update({neighbour: network.node[neighbour]['object'].get_budget_surplus() - network.node[neighbour]['object'].evaluate_placement_cost(self.get_resource_usage())}) for neighbour in network.neighbors(self.host_incumbent)]
			
			#network.node[self.host_incumbent]['object'].expulsion_complete()
			self.set_host(max(targets, key=targets.get))

		elif strategy == 'RANDOM':
			if len(neighbourhood) > 0:
				#network.node[self.host_incumbent]['object'].expulsion_complete()
				self.set_host(rnd.choice(neighbourhood))

		elif strategy == 'MAX_SLA_SURPLUS':
			# Problem
			prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

			# Assignment variable
			assign_vars = pulp.LpVariable.dicts("NeighbourSelection", neighbourhood, 0, 1, pulp.LpBinary)

			# Objective
			prob += pulp.lpSum( assign_vars[neighbour] * 
					self.evaluate_sla_95th_surplus_target_percent(network=network, target=neighbour) for neighbour in neighbourhood )
				
			# Only one selection
			prob += pulp.lpSum(assign_vars[neighbour] for neighbour in neighbourhood) == 1.0, ''

			# Solve it
			prob.solve()

			# Extract result
			TOL = 0.01
			dest = [neighbour for neighbour in neighbourhood if assign_vars[neighbour].varValue > TOL][0]

			#network.node[self.host_incumbent]['object'].expulsion_complete()
			self.set_host(dest)

		# Best effort
		elif strategy == 'LOCAL_BEST_EFFORT_EXP':
			# Compose link cost registry
			app_link_cost_registry = {neighbour: max(get_link_usage_cost(network=network, target=neighbour),1) for neighbour in neighbourhood}
			max_app_link_cost = max([cost for cost in app_link_cost_registry.itervalues()])

			# Problem
			prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

			# Assignment variable
			assign_vars = pulp.LpVariable.dicts("NeighbourSelection", neighbourhood, 0, 1, pulp.LpBinary)

			# Objective
			prob += pulp.lpSum(
				assign_vars[neighbour] * 
				math.exp( 
					(1.0)*
					(1.0 -( network.node[neighbour]['object'].evaluate_placement_cost(self.get_resource_usage(type='PRODUCTION'))*network.node[neighbour]['object'].epoch
							+ calculate_migration_cost(network, neighbour)
						)
					 / network.node[neighbour]['object'].get_budget_surplus()
					)
				)
				* 
				math.exp( 
					(10.0)*
					(
					self.evaluate_sla_95th_surplus_target_percent(network=network, target=neighbour)
					#self.evaluate_sla_95th_deficite_target(network=network, target=neighbour)
					#max([len(get_links(network=network, source=int(source), target=neighbour)) for source, quantity in applications[app_name].demand.iteritems() if quantity > 0])
					)
				)
				*
				math.exp((1.0)*(
					1.0 - app_link_cost_registry[neighbour]/get_link_usage_cost(network=network, target=self.host_incumbent)
					))
				for neighbour in neighbourhood
				)

			# Only one selection
			prob += pulp.lpSum(assign_vars[neighbour] for neighbour in neighbourhood) == 1.0, ''

			# Not in memory
			# for neighbour in neighbourhood:
			# 	for mem_node in self.get_placement_mem():
			# 		prob += assign_vars[neighbour] != mem_node, ''

			# # Positive budget surplus
			# for neighbour in neighbourhood:
			# 	prob += assign_vars[neighbour]*network.node[neighbour]['object'].get_budget_surplus_percent() 
			# 	- ( network.node[neighbour]['object'].evaluate_placement_cost(self.get_resource_usage(type='PRODUCTION'))*network.node[neighbour]['object'].epoch + calculate_migration_cost(network, neighbour))
			# 		>= network.node[self.host_incumbent]['object'].get_budget_surplus_percent() - 
			# 		network.node[self.host_incumbent]['object'].evaluate_placement_cost(self.get_resource_usage(type='PRODUCTION'))*network.node[self.host_incumbent]['object'].epoch, ''

			# # Improve SLA
			# for app_name in self.customers:
			# 	current_sla = applications[app_name].evaluate_sla_surplus_target(network=network, target=self.name)
			# 	for neighbour in neighbourhood:
			# 		prob += assign_vars[(app_name, neighbour)] * applications[app_name].evaluate_sla_surplus_target(network=network, target=neighbour) >= current_sla

			# Solve it
			prob.solve()

			# Extract result
			TOL = 0.01
			dest = [neighbour for neighbour in neighbourhood if assign_vars[neighbour].varValue > TOL][0]

			#network.node[self.host_incumbent]['object'].expulsion_complete()
			self.set_host(dest)

		elif strategy == 'LOCAL_BEST_EFFORT':
			# Compose link cost registry
			app_link_cost_registry = {neighbour: max(get_link_usage_cost(network=network, target=neighbour),1) for neighbour in neighbourhood}
			app_link_cost_registry[self.host_incumbent] = max(get_link_usage_cost(network=network, target=self.host_incumbent),1)

			dc_budget_surplus_registry = {neighbour:network.node[neighbour]['object'].get_budget_surplus() for neighbour in neighbourhood}

			max_app_link_cost = max([cost for cost in app_link_cost_registry.itervalues()])

			# Problem
			prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

			# Assignment variable
			assign_vars = pulp.LpVariable.dicts("NeighbourSelection", neighbourhood, 0, 1, pulp.LpBinary)

			# Objective
			prob += pulp.lpSum(
				assign_vars[neighbour] * 
				math.exp( 
					(1.0)*
					(1.0 -( network.node[neighbour]['object'].evaluate_placement_cost(self.get_resource_usage(type='PRODUCTION'))*network.node[neighbour]['object'].epoch
							+ calculate_migration_cost(network, neighbour)
						)
					 / dc_budget_surplus_registry[neighbour]
					)
				)
				* 
				math.exp( 
					(1.0)*
					(
					self.evaluate_sla_95th_surplus_target_percent(network=network, target=neighbour)
					#self.evaluate_sla_95th_deficite_target(network=network, target=neighbour)
					#max([len(get_links(network=network, source=int(source), target=neighbour)) for source, quantity in applications[app_name].demand.iteritems() if quantity > 0])
					)
				)
				*
				math.exp((0.0)*(
					1.0 - app_link_cost_registry[neighbour]/app_link_cost_registry[self.host_incumbent]
					))
				for neighbour in neighbourhood
				)

			# Only one selection
			prob += pulp.lpSum(assign_vars[neighbour] for neighbour in neighbourhood) == 1.0, ''

			# Not in memory
			# for neighbour in neighbourhood:
			# 	for mem_node in self.get_placement_mem():
			# 		prob += assign_vars[neighbour] != mem_node, ''

			# # Positive budget surplus
			# for neighbour in neighbourhood:
			# 	prob += assign_vars[neighbour]*network.node[neighbour]['object'].get_budget_surplus_percent() 
			# 	- ( network.node[neighbour]['object'].evaluate_placement_cost(self.get_resource_usage(type='PRODUCTION'))*network.node[neighbour]['object'].epoch + calculate_migration_cost(network, neighbour))
			# 		>= network.node[self.host_incumbent]['object'].get_budget_surplus_percent() - 
			# 		network.node[self.host_incumbent]['object'].evaluate_placement_cost(self.get_resource_usage(type='PRODUCTION'))*network.node[self.host_incumbent]['object'].epoch, ''

			# # Improve SLA
			# for app_name in self.customers:
			# 	current_sla = applications[app_name].evaluate_sla_surplus_target(network=network, target=self.name)
			# 	for neighbour in neighbourhood:
			# 		prob += assign_vars[(app_name, neighbour)] * applications[app_name].evaluate_sla_surplus_target(network=network, target=neighbour) >= current_sla

			# Solve it
			prob.solve()

			# Extract result
			TOL = 0.01
			dest = [neighbour for neighbour in neighbourhood if assign_vars[neighbour].varValue > TOL][0]

			#network.node[self.host_incumbent]['object'].expulsion_complete()
			self.set_host(dest)

