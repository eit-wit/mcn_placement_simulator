from resources.Resource import Resource

class DataCentre(Resource):
	
	TYPES = {
		'SMALL':{	
				'CPU': {'CAPACITY':750.0, 'COST':1.50},
				'STORAGE': {'CAPACITY':750.0, 'COST':1.50},
				'NET': {'CAPACITY':750.0, 'COST':1.0}
			},
		'MEDIUM':{	
				'CPU': {'CAPACITY':1500.0, 'COST':1.25},
				'STORAGE': {'CAPACITY':1500.0, 'COST':1.25},
				'NET': {'CAPACITY':1500.0, 'COST':1.125}
			},
		'LARGE':{	
				'CPU': {'CAPACITY':3000.0, 'COST':1.125},
				'STORAGE': {'CAPACITY':3000.0, 'COST':1.125},
				'NET': {'CAPACITY':3000.0, 'COST':1.25}
			},
		'HUGE':{	
				'CPU': {'CAPACITY':6000.0, 'COST':1.0},
				'STORAGE': {'CAPACITY':6000.0, 'COST':1.0},
				'NET': {'CAPACITY':6000.0, 'COST':1.5}
			}
		}

	EXPULSION_STRATEGIES = ['HEAVIEST','RANDOM']

	def __init__(self, name, resources, budget, epoch, expulsion_strategy):
		Resource.__init__(self, name, resources, budget, epoch, expulsion_strategy)