import logging 
import random as rnd
import pulp
import math
import networkx as nx

class NoCostFunc(object):
	def compute(self, usage):
		return 0

class FlowCounter(object):

	def __init__(self, eval_period):
		self.eval_period = int(eval_period)

		self.flow = []
		self.current_state = None

	def set_state(self, state={}):
		self.current_state = state

	def eval_state(self, state={}):
		if self.current_state == None:
			self.set_state(state)

		current_state_set = set(self.current_state.keys())
		new_state_set = set(state.keys())

		net_flow = 0.0

		# Outflow
		for entity in current_state_set.difference(new_state_set):
			net_flow -= self.current_state[entity]

		# Inflow
		for entity in new_state_set.difference(current_state_set):
			net_flow += state[entity]

		# Save the flow value
		self.flow.append(net_flow)

		# Set state to the new state
		self.set_state(state) 

	def get_net_flow(self):
		return sum(self.flow[-self.eval_period:])

class Resource(object):
	EXPULSION_STRATEGIES = [	'NONE', 
								'RANDOM', 
								'HEAVIEST', 
								'LOCAL_BEST_EFFORT', 
								'MAX_REMAINING', 
								'MAX_BUDGET_SURPLUS', 
								'PROPORTIONAL_RESPONSE', 
								'MAX_NET_SURPUS', 
								'MAX_NET_SURPUS_PERCENT', 
								'MAX_NET_SURPUS_PERCENTUAL', 
								'MAX_EQUAL_NET_SURPUS_PERCENTUAL', 
								'MIN_LOCAL_COST', 
								'MIN_DIFF',
								'LOCAL_BEST_EFFORT_EXP',
								'MAX_EQUAL_NET_SURPUS_PERCENTUAL_2']
	STRATEGIES_INCLUDE_SELF = [	'MAX_EQUAL_NET_SURPUS_PERCENTUAL',
								'LOCAL_BEST_EFFORT', 
								'PROPORTIONAL_RESPONSE', 
								'MAX_NET_SURPUS', 
								'MIN_LOCAL_COST', 
								'MAX_NET_SURPUS_PERCENT',
								'LOCAL_BEST_EFFORT_EXP',
								'MAX_EQUAL_NET_SURPUS_PERCENTUAL_2']
	STRATEGIES_SOLVER = [		'MAX_BUDGET_SURPLUS',
								'MAX_REMAINING', 
								'MAX_SURPLUS', 
								'LOCAL_BEST_EFFORT',
								'LOCAL_BEST_EFFORT_EXP', 
								'PROPORTIONAL_RESPONSE', 
								'MAX_NET_SURPUS', 
								'MAX_NET_SURPUS_PERCENT', 
								'MIN_LOCAL_COST', 
								'MAX_NET_SURPUS_PERCENTUAL', 
								'MIN_DIFF', 
								'MAX_EQUAL_NET_SURPUS_PERCENTUAL',
								'MAX_EQUAL_NET_SURPUS_PERCENTUAL_2']

	def __init__(self, name, resources, budget_factor, epoch, expulsion_strategy):
		self.name = name
		self.epoch = epoch
		self.expulsion_strategy = expulsion_strategy
		self.budget_factor = budget_factor

		self.resources = None
		self.max_cost = None
		self.capacity = None

		self.set_resources(resources)
		
		self.customers = {}

		self.time = 0.0

		self.nbr_customers_flow = FlowCounter(epoch)
		self.util_flow = FlowCounter(epoch)

		# State representation
		self.accumulated_cost = 0.0
		self.budget_surplus = 0.0
		self.est_budget_surplus = 0.0
		self.epoch_deficit = 0.0
		self.elapsed_time = 0.0
		self.prev_time = 0.0

		self.budget_vilolated = False

		self.momentary_cost = 0.0
		
		self.max_nbr_apps = 1.0

		self.time_of_violation = None

		self.mitigation_failed = False

		# Random seed
		rnd.seed(100)

	'''
	Workload
	'''
	def subject_demand(self, customer, resource_name, quantity):
		if resource_name in self.resources.keys():
			if customer not in self.customers:
				self.customers[customer] = {}
			if resource_name not in self.customers[customer]:
				self.customers[customer][resource_name] = 0

			self.customers[customer][resource_name] += quantity

	def provision_workload(): # If we want discrete resources
		pass

	'''
	Cost
	'''
	def calculate_usage(self):
		total_usage = {}

		for resource_name in self.resources:
			total_usage[resource_name] = 0

		for resources in self.customers.itervalues():
			for resource_name, usage in resources.iteritems():
				total_usage[resource_name] += usage

		return total_usage

	def calculate_cost_for_usage(self, usage, delta_time):
		cost = 0.0
		#print "Delta t = %f" % (delta_time)
		for resource_name, resource_usage in usage.iteritems():
			cost += self.resources[resource_name]['COST'] * delta_time * resource_usage

		return cost

	def calculate_cost(self, delta_time):
		total_usage = self.calculate_usage()
		self.momentary_cost = self.calculate_cost_for_usage(usage=total_usage, delta_time=delta_time) # All sub-routine outputs should be concatenated, not assigned in sub-routines
		return self.momentary_cost

	def compound_cost(self, time):
		self.accumulated_cost += self.calculate_cost(time)
		#logging.info('%s - Cost: %f' % (self.name, self.accumulated_cost))

	def compound_cost_per_resource(self, time):
		pass

	def interpolate_cost(self, epoch_remainder):
		total_usage = self.calculate_usage()
		return self.calculate_cost_for_usage(usage=total_usage, delta_time=epoch_remainder)

	# '''
	# Permutability - DEPRECATED
	# '''
	# def calculate_permutability(self, network, applications):
	# 	sla_map = {app_name: applications[app_name].get_sla_surplus() for app_name in self.customers}

	# 	neighbourhood = network.neighbors(self.name)

	# 	available_budget = sum([ network.node[neighbour]['object'].get_est_budget_surplus() 
	# 		for neighbour in neighbourhood if network.node[neighbour]['object'].get_est_budget_surplus()>0])+self.budget
	# 	total_mass = sum([ self.calculate_cost_for_usage(usage=applications[app_name].get_resource_usage(), delta_time=self.epoch) 
	# 		for app_name in self.customers ])
	# 	permutable_mass = sum([ self.calculate_cost_for_usage(usage=applications[app_name].get_resource_usage(), delta_time=self.epoch) 
	# 		for app_name in self.customers if 
	# 		 sum([1 for neighbour in neighbourhood if 
	# 		 	applications[app_name].evaluate_sla_surplus_target(network=network, target=neighbour) >= applications[app_name].get_sla_surplus() ]) 
	# 		# 	and  applications[app_name].migrating() != True
	# 			> 1  
	# 	])

	# 	#return min(permutable_mass,available_budget)/self.budget
	# 	if permutable_mass == 0.0:
	# 		return 1.0
	# 	else:
	# 		return min(1, total_mass/( permutable_mass * min(1, available_budget/permutable_mass) ))

	'''
	Permutability - NEW
	'''
	def calculate_permutability(self, network, applications):
		result = 0.0

		neighbours = network.neighbors(self.name)
		neighbour_budget_surplus_map = {neighbour_name : network.node[neighbour_name]['object'].get_est_budget_surplus() for neighbour_name in neighbours}

		for app_name in self.customers:
			for neighbour_name in neighbours:
				if applications[app_name].evaluate_sla_95th_surplus_target(network, neighbour_name) >= 0.0:
					load = self.calculate_cost_for_usage(usage=applications[app_name].get_resource_usage(), delta_time=self.epoch) 
					available = neighbour_budget_surplus_map[neighbour_name]

					if available > 0: 
						result += ( available - load ) / available

		return result

	'''
	Runtime modifications
	'''
	def modify_capacity(self,resources, network, applications):
		prev_capacity = self.capacity
		self.set_resources(resources)

		if prev_capacity < self.capacity:
			self.select_expulsion(self, network, applications, time)

	'''
	Behaviour
	'''
	def evaluate_budget(self, time, network, applications):
		self.budget_vilolated = False
		
		# Calculate net-flow
		self.nbr_customers_flow.eval_state(dict([(app_name,1) for app_name in self.customers]))
		self.util_flow.eval_state(dict([(app_name,sum([util for util in self.customers[app_name].itervalues()])/self.capacity*100) for app_name in self.customers]))

		# # Check resource utilisation
		# resource_over_utilised = False
		# for resource_name, resource_usage in self.calculate_usage().iteritems():
		# 	if resource_usage > self.resources[resource_name]['CAPACITY']:
		# 		resource_over_utilised = True
		# 		#print "%s: %s resource usage exceeded - %f %%" % (self.name, resource_name, resource_usage/self.resources[resource_name]['CAPACITY']*100)
				
		# if resource_over_utilised:
		# 	return self.select_expulsion(network, applications, time)

		# Time management
		delta_time = time - self.prev_time
		self.prev_time = time
		self.elapsed_time += delta_time

		# Compound cost
		self.compound_cost(delta_time)

		self.est_budget_surplus = self.budget - self.interpolate_cost(self.epoch)

		# Evaluate budget
		# If within epoch
		if self.elapsed_time <= self.epoch and self.accumulated_cost >= self.budget:
			if self.time_of_violation == None:
				self.time_of_violation = self.elapsed_time

			self.budget_surplus = 0.0
			self.epoch_deficit = self.interpolate_cost(self.epoch-self.time_of_violation)

			self.budget_vilolated = True

			return self.select_expulsion(network, applications, time)

		# If epoch expired
		elif self.elapsed_time == self.epoch:
			self.budget_surplus = self.budget - self.accumulated_cost

			self.renew_epoch()

		# Return no apps if noting is violated
		return []

	def expulsion_complete(self):
		self.renew_epoch()

	def renew_epoch(self):
		self.time_of_violation = None
		self.elapsed_time = 0.0
		self.accumulated_cost = 0.0

	'''
	Selection
	'''
	def select_expulsion(self, network, applications, time):
		def calculate_migration_cost(network, neighbour, app_name):
			if neighbour != self.name:
				return network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='MIGRATION'))
			else:
				return 0.0

		def get_links(network, source, target): # [TO-DO] add path cache
			links = []
			path = nx.shortest_path(G=network, source=source, target=target)
			for (n1,n2) in zip(path[0:],path[1:]):
				links.append(network.edge[n1][n2]['object'])

			return links


		def calculate_link_costs():
			pass

		assert self.expulsion_strategy in Resource.EXPULSION_STRATEGIES, '%s is not a valid expulsion strategy' % self.expulsion_strategy 

		result = []

		neighbourhood = [neighbour for neighbour in network.neighbors(self.name)+[self.name] if network.node[neighbour]['object'].get_budget_surplus()>0]

		if len(neighbourhood) == 0:
			return result

		'''
		Expulsion 
		'''
		# Random
		if self.expulsion_strategy == 'RANDOM':
			choices = [app_name for app_name in self.customers.keys() if applications[app_name].migrating() == 0]
			if len(choices) > 0: 
				result = [rnd.choice([app_name for app_name in self.customers.keys() if applications[app_name].migrating() == 0])]

		# None
		elif self.expulsion_strategy == 'NONE':
			pass

		# Heaviest
		elif self.expulsion_strategy == 'HEAVIEST': 
			cost = {}

			# Decision variables
			assign_vars = pulp.LpVariable.dicts("AppSelection", [app_name for app_name in self.customers], 0, 1, pulp.LpBinary)

			# Problem
			prob = pulp.LpProblem("HeaviestAppSelection", pulp.LpMaximize)

			# Objective
			for app_name, usage_resource in self.customers.iteritems():
				prob += pulp.lpSum(assign_vars[app_name]*sum([ usage * self.resources[resource_name]['COST'] for resource_name, usage in usage_resource.iteritems() ]))

			# Not migrating 
			for app_name in self.customers:
				prob += assign_vars[app_name]*applications[app_name].migrating() == 0, ''

			# There can be only one
			prob += pulp.lpSum([assign_vars[app_name] for app_name in self.customers]) == self.max_nbr_apps, ''

			# Solve the problem
			prob.solve()

			# Extract result
			TOL = 0.00001
			result = [app_name for app_name in self.customers if assign_vars[app_name].varValue > TOL]

		elif self.expulsion_strategy in ['MAX_REMAINING', 'MAX_SURPLUS', 'LOCAL_BEST_EFFORT', 'PROPORTIONAL_RESPONSE', 'MAX_NET_SURPUS', 'MIN_LOCAL_COST']:
			''' Common init '''

			# Decision variables
			assign_vars = pulp.LpVariable.dicts("NeighbourSelection", [(app_name, neighbour) for app_name in self.customers for neighbour in neighbourhood], 0, 1, pulp.LpBinary)

			''' Method specifics '''
			if self.expulsion_strategy == 'MAX_REMAINING':
				# Problem
				prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

				# Objective
				prob += pulp.lpSum( ( assign_vars[(app_name, neighbour)] * 
					(network.node[neighbour]['object'].get_budget_surplus() 
							- ( network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))
								+ calculate_migration_cost(network, neighbour, app_name)
							)
						)
					for neighbour in neighbourhood for app_name in self.customers ) ) 

				# Number of selections
				prob += pulp.lpSum(assign_vars[(app_name, neighbour)] for neighbour in neighbourhood for app_name in self.customers) <= self.max_nbr_apps, ''

			elif self.expulsion_strategy == 'MAX_SURPLUS':
				# Problem
				prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

				# Objective
				prob += pulp.lpSum(  assign_vars[(app_name, neighbour)] * 
					network.node[neighbour]['object'].get_budget_surplus_percent() 
					for neighbour in neighbourhood for app_name in self.customers ) 

				# Number of selections
				prob += pulp.lpSum(assign_vars[(app_name, neighbour)] for neighbour in neighbourhood for app_name in self.customers) <= self.max_nbr_apps, ''

			elif self.expulsion_strategy == 'LOCAL_BEST_EFFORT':
				# Problem
				prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMinimize)

				# Objective
				prob += pulp.lpSum(
					assign_vars[(app_name, neighbour)] * (

					math.exp( 
						(1.0/1.0)*
						( 1.0 - network.node[neighbour]['object'].get_budget_surplus_percent()/100.0 
							# - ( network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))*self.epoch
							# 	+ calculate_migration_cost(network, neighbour, app_name)*5
							# )
						)
					)
					* 
					math.exp( 
						(1.0/1.0)*
							(
						applications[app_name].evaluate_sla_95th_deficite_target(network=network, target=neighbour) / applications[app_name].sla
						#max([len(get_links(network=network, source=int(source), target=neighbour)) for source, quantity in applications[app_name].demand.iteritems() if quantity > 0])
							)
						)
					)
					for app_name in self.customers for neighbour in neighbourhood
					)

				# Number of selections
				prob += pulp.lpSum(assign_vars[(app_name, neighbour)] for neighbour in neighbourhood for app_name in self.customers) <= self.max_nbr_apps, ''

			elif self.expulsion_strategy == 'PROPORTIONAL_RESPONSE':
				margin = 0.5

				# Problem
				prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

				# Objective
				prob += pulp.lpSum(  assign_vars[(app_name, neighbour)] * 
					(network.node[neighbour]['object'].get_budget_surplus() 
							- ( network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))
								+ calculate_migration_cost(network, neighbour, app_name)
							)
						)
					for neighbour in neighbourhood for app_name in self.customers ) 

				# Budget ambition
				prob += pulp.lpSum(assign_vars[(app_name, neighbour)]*network.node[self.name]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))
				 for neighbour in neighbourhood for app_name in self.customers) <= self.epoch_deficit*(1+margin), ''

			elif self.expulsion_strategy == 'MAX_NET_SURPUS':
				budget_surpluses = dict([(neighbour,network.node[neighbour]['object'].get_budget_surplus()) for neighbour in neighbourhood])
				budget_surpluses[self.name] = self.budget

				# Problem
				prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMaximize)

				# Objective
				prob += pulp.lpSum(  assign_vars[(app_name, neighbour)] * 
					(budget_surpluses[neighbour]
							- ( network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))*self.epoch
								+ calculate_migration_cost(network, neighbour, app_name)
							)
						)
					for neighbour in neighbourhood for app_name in self.customers ) 

			elif self.expulsion_strategy == 'MIN_LOCAL_COST':
				budget_surpluses = dict([(neighbour,network.node[neighbour]['object'].get_budget_surplus()) for neighbour in neighbourhood])
				budget_surpluses[self.name] = self.budget

				# Problem
				prob = pulp.LpProblem("OptimalApplicationPlacement", pulp.LpMinimize)

				# Objective
				prob += pulp.lpSum(  assign_vars[(app_name, neighbour)] * 
					(network.node[neighbour]['object'].evaluate_placement_cost(applications[app_name].get_resource_usage(type='PRODUCTION'))*self.epoch
								+ calculate_migration_cost(network, neighbour, app_name)
								+ 0.0
							)
						
					for neighbour in neighbourhood for app_name in self.customers ) 

			''' Common constraints '''
			# Not in memory
			for neighbour in neighbourhood:
				for app_name in self.customers:
					for mem_node in applications[app_name].get_placement_mem():
						prob += assign_vars[(app_name, neighbour)] != mem_node, ''

			# One DC per app
			for app_name in self.customers:
				prob += pulp.lpSum(assign_vars[(app_name, neighbour)] for neighbour in neighbourhood) == 1.0, ''

			# Not migrating 
			for neighbour in neighbourhood:
				for app_name in self.customers:
					prob += assign_vars[(app_name, neighbour)]*applications[app_name].migrating() == 0.0, ''

			''' Solve '''
			# Solve it
			prob.solve()

			''' Results '''
			TOL = 0.01
			outcome = [(app_name, neighbour) for neighbour in neighbourhood for app_name in self.customers if assign_vars[(app_name, neighbour)].varValue > TOL]

			if len(outcome) > 0:
				for (app_name, neighbour) in outcome:
					applications[app_name].set_host(neighbour)

				self.renew_epoch()

		return result

	def evaluate_placement_cost(self, demand):
		return sum([usage * self.resources[resource_name]['COST'] for resource_name, usage in demand.iteritems()]) * self.epoch

	'''
	Metrics
	'''
	def get_mean_utilisation(self):
		usage_per_resource = self.calculate_usage()
		return sum( [(usage/self.resources[resource_name]['CAPACITY']) for resource_name, usage in usage_per_resource.iteritems()] ) / len(usage_per_resource) * 100

	def get_max_utilisation(self):
		return max( [(usage/self.resources[resource_name]['CAPACITY']) for resource_name, usage in self.calculate_usage().iteritems()] ) * 100

	def get_capacity(self):
		return sum( [resource['CAPACITY'] for resource in self.resources.itervalues()] )

	def get_budget_surplus_percent(self):
		return self.budget_surplus/self.budget * 100

	def get_est_budget_surplus_percent(self):
		return self.est_budget_surplus/self.budget * 100

	def get_budget_surplus(self):
		return self.est_budget_surplus
	
	def get_est_budget_surplus(self):
		return self.est_budget_surplus

	def get_budget_surplus(self):
		return self.budget_surplus

	def get_elapsed_time(self):
		return self.elapsed_time/self.epoch * 100

	def get_momentary_cost(self):
		return self.momentary_cost

	def get_max_cost(self):
		return self.max_cost

	def get_nbr_apps_net_flow(self):
		return self.nbr_customers_flow.get_net_flow()

	def get_util_net_flow(self):
		return self.util_flow.get_net_flow()

	def get_capacity(self):
		return self.capacity

	def get_mitigation_outcome(self):
		return self.mitigation_failed

	'''
	Attribute getters
	'''
	def get_latency(self, direction):
		return 0.0

	def get_properties(self, prop):
		result = {}
		for resource_name, resource_properties in self.resources.iteritems():
			result[resource_name] = resource_properties[prop]

		return result

	'''
	External invocations
	'''
	def clear_all_demand(self):
		self.customers = {}

	def set_resources(self, resources):
		self.resources = resources
		self.max_cost = sum([resource['CAPACITY'] * resource['COST'] for resource in self.resources.values()])
		self.capacity = sum([resource['CAPACITY'] for resource in self.resources.values()])