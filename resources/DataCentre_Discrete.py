from resources.Resource_Discrete import Resource_Discrete

class DataCentre_Discrete(Resource_Discrete):
	VM_TYPES = {
		'SMALL':{
				'CPU': 3.0,
				'STORAGE': 3.0,
				'NET': 3.0
			}
		}

	TYPES = {
		'NONE':{
				'CPU': {'CAPACITY':0.0, 'COST':0.0},
				'STORAGE': {'CAPACITY':0.0, 'COST':0.0},
				'NET': {'CAPACITY':0.0, 'COST':0.0}
			},
		'SMALL':{
				'CPU': {'CAPACITY':750.0, 'COST':1.25},
				'STORAGE': {'CAPACITY':750.0, 'COST':1.25},
				'NET': {'CAPACITY':750.0, 'COST':1.25}
			},
		'MEDIUM':{	
				'CPU': {'CAPACITY':1500.0, 'COST':1.125},
				'STORAGE': {'CAPACITY':1500.0, 'COST':1.125},
				'NET': {'CAPACITY':1500.0, 'COST':1.125}
			},
		'LARGE':{	
				'CPU': {'CAPACITY':3000.0, 'COST':1.0},
				'STORAGE': {'CAPACITY':3000.0, 'COST':1.0},
				'NET': {'CAPACITY':3000.0, 'COST':1.0}
			},
		'HUGE':{	
				'CPU': {'CAPACITY':6000.0, 'COST':0.825},
				'STORAGE': {'CAPACITY':6000.0, 'COST':0.825},
				'NET': {'CAPACITY':6000.0, 'COST':0.825}
			}
		}

	def __init__(self, name, resources, vm_type, budget_factor, epoch, expulsion_strategy):
		Resource_Discrete.__init__(self, name, resources, vm_type, budget_factor, epoch, expulsion_strategy)