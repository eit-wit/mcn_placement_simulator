import networkx as nx
import datetime	
import random as rnd
import numpy as np
import os
import shutil

from support_classes.ScenarioLoader import ScenarioLoader
from support_classes.WorkloadSource import WorkloadSource
#from intelligent_workload_generator import generate_workload as workload_generator_static
#from intelligent_workload_generator_mobile import generate_workload as workload_generator_mobile
from universal_workload_generator import workload_generator, workload_generator_static, workload_generator_mobile
from support_classes.FatTreeNetworkGeneratorManual import FatTreeNetworkGeneratorManual
from support_classes.TreeNetworkGeneratorManual import TreeNetworkGeneratorManual
from support_classes.ErdosRenyiNetworkGenerator import ErdosRenyiNetworkGenerator
from resources.Application import Application
from resources.DataCentre_Discrete import DataCentre_Discrete
from resources.Link import Link
from opt_app_placement_discrete import opt_app_placement_discrete

DC_TYPE_ID_TO_NAME_MAP={3:'HUGE', 2:'LARGE', 1:'MEDIUM', 0:'SMALL'}
DC_TYPE_NAME_TO_ID_MAP={type_name:type_id for type_id, type_name in DC_TYPE_ID_TO_NAME_MAP.iteritems()}
LINK_TYPES_MAP={2:'LARGE', 1:'MEDIUM', 0:'SMALL'}

def main(batch_file_path, workload_path, output_path):
	scenario_loader = ScenarioLoader(batch_file_path=batch_file_path)

	# Results directory		
	if not os.path.exists(output_path):
		os.mkdir(output_path)

	'''
	Print global properties
	'''
	print '-- Initiating simulation -- '
	print 'Start time: %s ' % datetime.datetime.now()

	# Set seeds
	# rnd.seed(0) # Remove use of random
	# np.random.seed(seed=0)
	for batch_config in scenario_loader.get_configuration():
		# Unpack Batch configuration
		duration = batch_config['DURATION']
		batch_name = batch_config['BATCH_NAME']
		nbr_runs = int(batch_config['NBR_RUNS'])
		nbr_apps = int(batch_config['APPLICATIONS']['NBR'])
		placement_mem_size = batch_config['APPLICATIONS']['PLACEMENT_MEM_SIZE']
		epoch = int(batch_config['EPOCH'])
		initial_placement_method = batch_config['INITIAL_PLACEMENT']
		excl_init_placement = batch_config['EXCL_INIT_PLACEMENT']
		dc_capacity_change_event = batch_config['DC_CAPACITY_CHANGE_EVENT']
		budget_factor = batch_config['BUDGET_FACTOR']
		sla_padding = batch_config['APPLICATIONS']['SLA_PADDING']
		network_type = batch_config['NETWORK']['TYPE']
		do_generate_workload = batch_config['WORKLOAD']['GENERATE'].lower() in ("yes", "true")
		global_scale = batch_config['WORKLOAD']['CONFIG']['GLOBAL_SCALE']
		workload_type = batch_config['WORKLOAD']['TYPE']
		migration_sla_buffer = batch_config['APPLICATIONS']['MIGRATION_SLA_BUFFER']
		scale_per_type = {int(dc_type_id):scale for dc_type_id, scale in batch_config['WORKLOAD']['SCALE_PER_TYPE'].iteritems()}

		print '---------------------------- '
		print '/// Batch: %s ///' % (batch_name)

		'''
		Initialise support functions 
		'''
		batch_result_path = "%s/%s" % (output_path,batch_name)
		if not os.path.exists(batch_result_path):
			os.mkdir(batch_result_path)

		''' Copy configuration file to batch folder '''
		(path, file) = os.path.split(batch_file_path)
		shutil.copyfile(batch_file_path, os.path.join(batch_result_path, file))

		app_random_placement = []
		app_semi_random_placement = []

		''' Network generator '''
		print " === Generating a %s network" % network_type.lower().replace("_"," "),
		if network_type == 'FAT_TREE':
			network_generator = FatTreeNetworkGeneratorManual(config=batch_config['NETWORK']['CONFIG'], dc_types_map=DC_TYPE_ID_TO_NAME_MAP, link_type_map=LINK_TYPES_MAP)
		elif network_type == 'BAL_TREE':
			network_generator = TreeNetworkGeneratorManual(config=batch_config['NETWORK']['CONFIG'], dc_types_map=DC_TYPE_ID_TO_NAME_MAP, link_type_map=LINK_TYPES_MAP)
		elif network_type == 'ERDOS_RENYI':
			network_generator = ErdosRenyiNetworkGenerator(config=batch_config['NETWORK']['CONFIG'], dc_types_map=DC_TYPE_ID_TO_NAME_MAP, link_type_map=LINK_TYPES_MAP)
		else:
			print " *** Topology type: %s, not supported *** " % network_type

		network = network_generator.generate_network()
		leafs = network_generator.get_leaf_nodes()
		roots = network_generator.get_root_nodes()
		dc_depth_map = network_generator.get_dc_depth_map()


		print "[DONE]"

		''' Workload generator '''
		if do_generate_workload:
			print " === Generating a %s worload ... " % workload_type.lower(),
			# workload scale parameters aggregation
			
			load_per_type = { dc_type_id : max([resource['CAPACITY'] for resource in DataCentre_Discrete.TYPES[dc_type_name].itervalues() ]) for dc_type_id, dc_type_name in DC_TYPE_ID_TO_NAME_MAP.iteritems() }
			
			if workload_type == "STATIC":
				workload_generator = workload_generator_static(network=network, leafs=leafs, roots=roots, nbr_apps=nbr_apps)
			elif workload_type == "MOBILE":
				workload_generator = workload_generator_mobile(network=network, leafs=leafs, roots=roots, nbr_apps=nbr_apps)

			workload_generated, workload_path = workload_generator.generate(
					load_per_type=load_per_type, 
					scale_per_type=scale_per_type, 
					global_scale=global_scale, 
					duration=duration, 
					boundary_bounce= network_type != 'ERDOS_RENYI')

			print "[DONE]"
		else:
			workload_path = os.path.join('workloads',batch_config['WORKLOAD']['PATH'])

		print " === Loading workload ... ",
		# Workload parser
		workload_source = WorkloadSource(workload_path)
		max_app_spread, sla_per_type = workload_source.get_stats()

		print "\tSLA per type: %s" % sla_per_type

		print "[DONE]"

		# ''' Saving type mappings '''
		# print " === Saving application types to file ... ",
		# # Acumulate per type 
		# type_to_app = {app_type:[] for app_type in Application.TYPES}
		# for i in range(nbr_apps):
		# 	 type_to_app[application_types[i]].append(i)
		# with open( batch_result_path+"/app_type.csv", "w") as myfile:
		# 	for line in sorted(type_to_app):
		# 		myfile.write(','.join(map(str, line)) + '\r')
		# print "[DONE]"

		print " === Saving node depth types to file ... ",
		# Acumulate per type 
		dc_to_depth = [depth for node, depth in sorted(dc_depth_map.iteritems())]
		with open( batch_result_path+"/dc_depth.csv", "w") as myfile:
			myfile.write(','.join(map(str, dc_to_depth)))
		print "[DONE]"

		'''
		Run the experiment
		'''
		for properties in scenario_loader.get_scenario():
			# Set seeds
			# rnd.seed(0)
			# np.random.seed(seed=0)

			# Unpack parameters
			scenario_name = properties['SCENARIO_NAME']
			budget_strategy = properties['BUDGET_VILOATION_MITIGATION_STRATEGY']
			sla_strategy = properties['SLA_VILOATION_MITIGATION_STRATEGY']
			expulsion_strategy = properties['EXPULSION_STRATEGY']

			local_nbr_runs = properties['NBR_RUNS'] if 'NBR_RUNS' in properties else nbr_runs

			# Save results
			scenario_result_path = '%s/%s' % (batch_result_path, scenario_name)
			if not os.path.exists(scenario_result_path):
				os.mkdir(scenario_result_path)

			for index_run in range(local_nbr_runs):
				print' -- Running scenario : %s %i/%i' % (scenario_name, (index_run+1), local_nbr_runs)

				'''
				Initialise the infrastructure
				'''
				# Create applications

				print " === Assigning application types ... ",
				application_types = [Application.TYPES[np.random.choice(Application.TYPES.keys())] for i in range(nbr_apps)]
				#application_types = [Application.TYPES['SYMMETRIC'] for i in range(nbr_apps)]
				with open( scenario_result_path+"/app_types_"+str(index_run)+".csv", "w") as myfile:
					myfile.write(','.join(map(str, application_types)))
				print "[DONE]"

				#applications = [Application(i, application_types[i] , None, max_app_spread, placement_mem_size) for i in range(nbr_apps)]
				#applications = [Application(i, Application.TYPES['SYMMETRIC'], sla_range, max_app_spread, placement_mem_size) for i in range(nbr_apps)]
				applications = [Application(i, application_types[i], sla_per_type, sla_padding, max_app_spread, placement_mem_size, migration_sla_buffer) for i in range(nbr_apps)]

				# Instantiate network
				if budget_strategy == 'OPTIMAL':
					bf = 5.0
				else:
					bf = budget_factor

				network = network_generator.initialise_nodes(budget_factor=bf, epoch=epoch, expulsion_strategy=expulsion_strategy)

				'''
				Set up data collection
				'''
				# Initialise result vector
				results = {
					'UTILISATION_DC': np.empty((0,len(network.node)), float),
				 	'UTILISATION_LINK': np.empty((0,len(network.edges())), float),
				 	'ALLOCATION_SYSTEM': np.empty(0, int),
				 	'ALLOCATION': np.empty((0,len(network.node)), float),
					'BUDGET_SURPLUS': np.empty((0,len(network.node)), float),
					'BUDGET_SURPLUS_ESTIMATED': np.empty((0,len(network.node)), float),
					'EPOCH_DURATION': np.empty((0,len(network.node)), float),
					'FAILED_MITIGATIONS_DC': np.empty(0, int),
					'NET_APP_FLOW': np.empty((0,len(network.node)), float),
					'NET_UTIL_FLOW': np.empty((0,len(network.node)), float),
					'COST': np.empty((0,len(network.node)+len(network.edges())), int),
					'SLA_SURPLUS': np.empty((0,len(applications)), float),
					'MIGRATING': np.empty((0,len(applications)), int),
					'PLACEMENT': np.empty((0,len(applications)), int),
					'SLA_DURATION': np.empty((0,len(applications)), int),
					'BUDGET_VILOATIONS': np.empty(0, int),
					'FAILED_MITIGATIONS_APP': np.empty((0,len(applications)), int),
					'SLA_VILOATIONS': np.empty(0, int),
					'PERMUTABILITY' : np.empty((0,len(network.node)), float),
				}

				'''
				Initial conditions
				'''
				# Initial app placement	
				available_dcs = range(len(network.node))
				for dc in excl_init_placement:
					available_dcs.remove(dc)

				# Calculate capacity distribution
				capacity_dist = [network.node[dc_name]['object'].get_capacity() for dc_name in available_dcs]

				total_capacity = sum(capacity_dist)

				capacity_dist = [float(cap)/float(total_capacity) for cap in capacity_dist]

				max_cost = sum([res['object'].get_max_cost() for res in network.node.itervalues()]) + sum([network.edge[n1][n2]['object'].get_max_cost() for (n1,n2) in network.edges()])

				if initial_placement_method == 'RANDOM':
					if len(app_random_placement) == 0:
						app_random_placement = np.random.choice(available_dcs, nbr_apps)
					[app.set_host( app_random_placement[int(app.name)] ) for app in applications]
				elif initial_placement_method == 'SEMI_RANDOM':
					if len(app_semi_random_placement) == 0:
						app_semi_random_placement = np.random.choice(available_dcs, size=nbr_apps, p=capacity_dist)
					[app.set_host( app_semi_random_placement[int(app.name)] ) for app in applications]
				
				# Time loop - Time collected from workload
				for t, workload in workload_source.next():
					#logger.info('Time: %s' % t)

					# Subject workload
					for app_name, demand in workload.iteritems():
						if int(app_name) < len(applications):
							applications[int(app_name)].update_demand(demand)

					''' Check if event is due '''
					for time, events in dc_capacity_change_event.iteritems():
						if float(time) == float(t):
							for event_type, event_desc in events.iteritems():
								if event_type == "DETERMINISTIC":
									for dc_name, to_dc_type in event_desc.iteritems():
										print " - Setting DC%s to capacity type %s" % (int(dc_name), to_dc_type)
										network.node[int(dc_name)]['object'].set_resources(DataCentre_Discrete.TYPES[to_dc_type])
								elif event_type == "STOCHASTIC":
									for from_dc_type, to_dc_type in event_desc.iteritems():
										candidates = [dc_id for dc_id, node in network.node.iteritems() if node['type']==DC_TYPE_NAME_TO_ID_MAP[from_dc_type]]
										candiate = np.random.choice(candidates)
										print " - Setting DC%s to capacity type %s" % (int(candiate), to_dc_type)
										network.node[candiate]['object'].set_resources(DataCentre_Discrete.TYPES[to_dc_type])
								else:
									print " [E] - Unknown event type: %s" % (event_type)

					if t <= 0.0:
						''' Save app demand type '''
						app_demand_type = [app.get_demand_type() for app in applications]
						with open(scenario_result_path+"/app_demand_types_"+str(index_run)+".csv", "w") as myfile:
							myfile.write(','.join(map(str, app_demand_type)))

						if budget_strategy == 'OPTIMAL' or initial_placement_method == 'OPTIMAL':
							print "\tOptimising ... ",
							placemet_decision = opt_app_placement_discrete(applications=applications, network=network, demand=workload, epoch=epoch)

							for app_name, placement in placemet_decision.iteritems():
								applications[app_name].force_set_host(placement)

							print "[DONE]"

					if workload_type == "MOBILE" and budget_strategy == 'OPTIMAL':
						print "\tOptimising ... ",
						placemet_decision = opt_app_placement_discrete(applications=applications, network=network, demand=workload, epoch=epoch)

						for app_name, placement in placemet_decision.iteritems():
							applications[app_name].force_set_host(placement)

						print "[DONE]"

					# Clear resource usage
					# DCs
					for res in network.node.itervalues():
						res['object'].clear_all_demand()

					# Links
					for (n1,n2) in network.edges():
						network.edge[n1][n2]['object'].clear_all_demand()

					# Propagate workload
					for app in applications:	
						app.propagate_workload(network=network, time=t) # Include in initiation

					budget_breakers = set()
					# Evaluation - DC budget
					node_indices = network.nodes()
					rnd.shuffle(node_indices)
					edges = network.edges()
					rnd.shuffle(edges)

					for node_index in node_indices:
						network.node[node_index]['object'].evaluate_budget(time=t, network=network, applications=applications)
					for (n1,n2) in edges:
					#for (n1,n2) in network.edges():
						[budget_breakers.add(applications[app]) for app in network.edge[n1][n2]['object'].evaluate_budget(time=t, network=network, applications=applications)] 
					#logger.info('Budget viloators: %s' % [app.name for app in budget_breakers])

					sla_breakers = set()
					# Evaluation - App performance

					app_indices = range(len(applications))
					rnd.shuffle(app_indices)
					#[sla_breakers.add(app) for app in applications if app.evaluate_sla_95th(network=network)] 
					[sla_breakers.add(applications[app_index]) for app_index in app_indices if applications[app_index].evaluate_sla_95th(network=network)] 
					#logger.info('SLA viloators: %s' % [app.name for app in sla_breakers])

					budget_breakers = [violator for violator in budget_breakers if violator not in sla_breakers]

					if budget_strategy != 'OPTIMAL':

						# Actuate
						for app in budget_breakers:
							#logger.info('Budget viloation: Re-evaluating application %s' % app.name)
							app.re_evaluate(network=network, strategy=budget_strategy) 

						for app in sla_breakers:
							#logger.info('SLA viloation: Re-evaluating application %s' % app.name)
							app.re_evaluate(network=network, strategy=sla_strategy)
					

					# Collect run-time measurements
					results['UTILISATION_DC'] = np.append(results['UTILISATION_DC'], 
						[[res['object'].get_mean_utilisation() for res in network.node.itervalues()]], axis=0)
					results['ALLOCATION_SYSTEM'] = np.append(results['ALLOCATION_SYSTEM'], 
						[sum([res['object'].get_nbr_allocations() for res in network.node.itervalues()])/total_capacity*100], axis=0)
					results['ALLOCATION'] = np.append(results['ALLOCATION'], 
						[[res['object'].get_allocation() for res in network.node.itervalues()]], axis=0)
					results['FAILED_MITIGATIONS_DC'] = np.append(results['FAILED_MITIGATIONS_DC'], 
						[sum([1 for res in network.node.itervalues() if res['object'].get_mitigation_outcome()])], axis=0)
					results['FAILED_MITIGATIONS_APP'] = np.append(results['FAILED_MITIGATIONS_APP'], 
						[[(int(app.get_mitigation_outcome())+1)%2 for app in applications]], axis=0)
					results['UTILISATION_LINK'] = np.append(results['UTILISATION_LINK'], 
						[[network.edge[n1][n2]['object'].get_mean_utilisation() for (n1,n2) in network.edges()]], axis=0)
					results['BUDGET_SURPLUS'] = np.append(results['BUDGET_SURPLUS'], 
						[[res['object'].get_budget_surplus_percent() for res in network.node.itervalues()]], axis=0)
					results['BUDGET_SURPLUS_ESTIMATED'] = np.append(results['BUDGET_SURPLUS_ESTIMATED'], 
						[[res['object'].get_est_budget_surplus_percent() for res in network.node.itervalues()]], axis=0)
					results['PERMUTABILITY'] = np.append(results['PERMUTABILITY'], 
						[[res['object'].calculate_permutability(network=network, applications=applications) for res in network.node.itervalues()]], axis=0)
					results['SLA_DURATION'] = np.append(results['SLA_DURATION'],
						[[app.get_accumulated_time_sla() for app in applications]], axis=0)
					results['EPOCH_DURATION'] = np.append(results['EPOCH_DURATION'], 
						[[res['object'].get_elapsed_time() for res in network.node.itervalues()]], axis=0)
					results['NET_APP_FLOW'] = np.append(results['NET_APP_FLOW'], 
						[[res['object'].get_nbr_apps_net_flow() for res in network.node.itervalues()]], axis=0)
					results['NET_UTIL_FLOW'] = np.append(results['NET_UTIL_FLOW'], 
						[[res['object'].get_util_net_flow() for res in network.node.itervalues()]], axis=0)
					results['SLA_SURPLUS'] = np.append(results['SLA_SURPLUS'], 
						[[app.get_sla_surplus() for app in applications]], axis=0)
					results['MIGRATING'] = np.append(results['MIGRATING'],
						[[int(app.migrating()) for app in applications]], axis=0)
					results['PLACEMENT'] = np.append(results['PLACEMENT'],
						[[dc_depth_map[int(app.get_placement())] for app in applications]], axis=0)
					results['BUDGET_VILOATIONS'] = np.append(results['BUDGET_VILOATIONS'], 
						[sum([1 for res in network.node.itervalues() if res['object'].budget_violated()])], axis=0)
					results['SLA_VILOATIONS'] = np.append(results['SLA_VILOATIONS'], 
						[sum([1 for app in applications if app.sla_viloated()])], axis=0)
					results['COST'] = np.append(results['COST'], 
						[[(res['object'].get_momentary_cost()/(max_cost))*100 for res in network.node.itervalues()]+[(network.edge[n1][n2]['object'].get_momentary_cost()/max_cost)*100 for (n1,n2) in network.edges()]], axis=0)

				print " - Mean allocation : %f" % (np.mean(results['UTILISATION_DC']))

				for measurement_name, data  in results.iteritems():
					# Make sure folder has been created
					meas_result_path = os.path.abspath('%s/%s' % (scenario_result_path, measurement_name))
					if not os.path.exists(meas_result_path):
						os.mkdir(meas_result_path)

					np.savetxt('%s/%s_%i.csv' % (meas_result_path, measurement_name, index_run), data, delimiter=',', fmt='%1.10f')

	print '\rFatto elaborazione'
	print '[DONE]'
 
if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(description='Telco-cloud simulator')

	parser.add_argument('-p', 
		metavar='P',
		type=str, 
		default='configuration/live_scenario_static_fat.json',
		#default='configuration/budget_sensitivity_scenario.json',
		#default='configuration/verification_scenario_local_best_effort.json',
		#default='configuration/verification_optimal.json',
		#default='configuration/verification_max_budget_surplus_random.json',
		help='Path to batch file')

	parser.add_argument('-w',
		metavar='W',
		type=str, 
		#default='workloads/verification_workload_a_150_t_200_d_10000_mobile_hetero.js on',
		default='workloads/verification_workloada_150_t_200_d_125000_stationary_hetero.json',
		#default='workloads/verification_workloada_150_t_10_d_125000_stationary_hetero.json',
		help='Workload path')

	parser.add_argument('-o', 
		metavar='O',
		type=str, 
		default='results',
		help='Output path')

	args = parser.parse_args()
	
	main(args.p, args.w, args.o)